import React, { Component } from 'react';

class WhiteListScreen extends Component {
  componentDidMount () {
    let self = this;
    let $ = window.$;

    let check = setInterval(() => {
      if($ && $('#countdown').countdown) {
        self.startTimer()
        clearInterval(check)
      }
    })

    let check2 = setInterval(() => {
      if($("#hs-form-container").length && window.hbspt) {
        self.startForm()
        clearInterval(check2)
      }
    })

  }

  startTimer() {
    let timestamp = new Date(2019, 1, 15)
    window.$('#countdown').countdown({ timestamp })
  }

  startForm () {
    window.hbspt.forms.create({
      portalId: '4513318',
      formId: 'b86d363c-72e8-4652-8cec-1d651902e23d',
      target: "#hs-form-container",
      onFormSubmit: function($form) {}
    });

    let check = setInterval(() => {
      if(window.$('#hs-form-container .input input').length) {
        window.$('#hs-form-container .input input').attr('placeholder', 'Enter email address')
        clearInterval(check)
      }
    })
  }

  render() {
    return (
      <section className="whitelist-info">
        <div className="wrapper">
          <div className="whitelist-info__wrap">
            <div className="whitelist-info__text">
              <h2>Stay one step ahead <br /> and whitelist now for <br /> the next round.</h2>
              <a target="_blank" href="https://tsd.network/whitelist" className="button whitelist-info__btn">JOIN THE WHITELIST</a>
            </div>
            <div className="whitelist-info__time">
              <div className="whitelist-info__title">Private round ends in</div>
              <div id="countdown">
                  <div className="text-countdown">Days</div>
                  <div className="text-countdown">HRS</div>
                  <div className="text-countdown">Mins</div>
                  <div className="text-countdown">Secs</div>
              </div>
              {/* <ul className="whitelist-time-list">
                <li className="whitelist-time-list__item">
                  <div className="whitelist-time-list__value">90</div>
                  <div className="whitelist-time-list__type">DAYS</div>
                </li>
                <li className="whitelist-time-list__item">
                  <div className="whitelist-time-list__value">17</div>
                  <div className="whitelist-time-list__type">HRS</div>
                </li>
                <li className="whitelist-time-list__item">
                  <div className="whitelist-time-list__value">28</div>
                  <div className="whitelist-time-list__type">MINS</div>
                </li>
                <li className="whitelist-time-list__item">
                  <div className="whitelist-time-list__value">56</div>
                  <div className="whitelist-time-list__type">SECS</div>
                </li>
              </ul> */}
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default WhiteListScreen;
