import React, { Component } from 'react';

import Whitepaper from '../../assets/img/icons/Whitepaper.svg'
import Pitchdeck from '../../assets/img/icons/Pitchdeck.svg'
import Platformdeck from '../../assets/img/icons/Platformdeck.svg'
import CoinOffering from '../../assets/img/icons/Coin-Offering.svg'
import FundsAllocationBreakdown from '../../assets/img/icons/Funds-Allocation-Breakdown.svg'
import TokenSalesAgreement from '../../assets/img/icons/Token-Sales-Agreement.svg'
import ReferralProgram from '../../assets/img/icons/Referral-Program.svg'
import BountyProgram from '../../assets/img/icons/Bounty-Program.svg'
import IcoratingAudit from '../../assets/img/icons/Icorating-Audit.svg'
import LegalOpinion from '../../assets/img/icons/Legal-Opinion.svg'

class MainScreen extends Component {
  constructor (props) {
    super(props)

    this.state = { displayCountryModal: false }
  }

  componentDidMount () {
    let $ = window.$;
    let self = this;

    let check = setInterval(() => {
      if ($('.documents-info .documents-info__more')) {
        $('.documents-info .documents-info__more').click(function(){
          $('.documents-info-list').addClass('showed-all');
          $(this).hide();
          return false;
        });
        clearInterval(check)
      }
    })
  }

  hideModal (event) {
    this.setState({ displayCountryModal: true })
    event.preventDefault()
    return false;
  }

  render() {
    fetch('https://geoip-db.com/jsonp')
      .then(response => {
        const data = response.json()
        if(data.country_name === 'Australia') {
          this.setState({ displayCountryModal: true })
        }
      })

    return (
      <section className="documents-info">
        <div id="resources" className="section-nav"></div>
        <div className="wrapper wrapper_small">
          <div className="documents-info__text">
            <div className="subtitle">Resources</div>
            <h2>Whitepaper and documents.</h2>
          </div>
          <div className="documents-info-list__wrap">
            <ul className="documents-info-list">
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={Whitepaper} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Whitepaper</div>
                  <a target="_blank" href="https://s3-ap-southeast-2.amazonaws.com/tsd-external-assets/Downloads/Transcendence_Whitepaper.pdf" className="documents-info-list__btn">Download PDF</a>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={Pitchdeck} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Pitch Deck</div>
                  <a target="_blank" href="https://s3-ap-southeast-2.amazonaws.com/tsd-external-assets/Downloads/Transcendence_PitchDeck.pdf" className="documents-info-list__btn">Download PDF</a>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={Platformdeck} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Platform Deck</div>
                  <a target="_blank" href="https://s3-ap-southeast-2.amazonaws.com/tsd-external-assets/Downloads/Transcendence_PlatformDeck.pdf" className="documents-info-list__btn">Download PDF</a>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={CoinOffering} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Token Offering</div>
                  <a target="_blank" href="https://s3-ap-southeast-2.amazonaws.com/tsd-external-assets/Downloads/Transcendence_TokenOffering.pdf" className="documents-info-list__btn">Download PDF</a>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={FundsAllocationBreakdown} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Funds Allocation <br /> Breakdown</div>
                  <a target="_blank" href="https://s3-ap-southeast-2.amazonaws.com/tsd-external-assets/Downloads/Transcendence_FundsAllocationBreakdown.pdf" className="documents-info-list__btn">Download PDF</a>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={TokenSalesAgreement} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Token Sales <br /> Agreement</div>
                  <a target="_blank" href="https://s3-ap-southeast-2.amazonaws.com/tsd-external-assets/Downloads/Transcendence_TokenSalesAgreement.pdf" className="documents-info-list__btn">Download PDF</a>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={ReferralProgram} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Referral Program</div>
                  <span  className="documents-info-list__res">Coming soon</span>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={BountyProgram} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Bounty Program</div>
                  <span  className="documents-info-list__res">Coming soon</span>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={IcoratingAudit} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Icorating Audit</div>
                  <span  className="documents-info-list__res">Coming soon</span>
                </div>
              </li>
              <li className="documents-info-list__item">
                <div className="documents-info-list__img">
                  <img src={LegalOpinion} alt="" />
                </div>
                <div className="documents-info-list__info">
                  <div className="documents-info-list__title">Legal Opinion</div>
                  <span  className="documents-info-list__res">Coming soon</span>
                </div>
              </li>
            </ul>
            <div className="document-country" style={{ display: this.state.displayCountryModal ? 'block' : 'none' }}>
              <div className="document-country__cont">
                  <div className="document-country__title">The Transcendence TGE is not open to purchasers in Australia.</div>
                  <div className="document-country__text">Please confirm using the buttons below whether or not you are located in Australia.</div>
                  <div className="document-country__button">
                      <a href="#" className="button button_without-shadow" onClick={this.hideModal.bind(this)}>I’M NOT IN AUSTRALIA</a>
                      <span className="button button_without-shadow button_disabled">I’M NOT IN AUSTRALIA</span>
                  </div>
              </div>
            </div>
          </div>
          <a href="#" className="documents-info__more">See more
            <div className="documents-info__icon">
              <i className="icon-arrow_bottom"></i>
            </div>
          </a>
          <a target="_blank" href="https://s3-ap-southeast-2.amazonaws.com/tsd-external-assets/Downloads/Transcendence_AllDocuments.zip" className="documents-info__download">DOWNLOAD ALL DOCUMENTS</a>
          <div className="documents-info__subscribe-btn">Subscribe to our newsletter
            <div className="documents-info__icon">
              <i className="icon-arrow_bottom"></i>
            </div>
          </div>
        </div>
        <div className="wrapper">
          <div className="documents-info-subscribe">
            <div id="hs-form-container"></div>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
