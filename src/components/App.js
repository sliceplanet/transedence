import React, { Component } from 'react';
import { HashRouter as Router, Route, StaticRouter, Switch,  Redirect} from "react-router-dom";

import Header from './ui/Header'
import Footer from './ui/Footer'
import Cookie from './ui/Cookie'

import HomePage from './pages/home'
import TokenPage from './pages/token'
import PlatformPage from './pages/platform'
import FaqPage from './pages/faq'
import TeamPage from './pages/team'
import CookiesPolicyPage from './pages/cookies-policy'
import PrivacyPolicyPage from './pages/privacy-policy'
import DisclaimersPage from './pages/disclaimers'
import WhiteListPage from './pages/whitelist'
import PageNotFound from './pages/page-not-found'

import '../assets/scss/style.scss'

class App extends Component {
  componentDidMount () {
    let self = this;

    let check = setInterval(() => {
      if (window.WOW && window.$('.wow').length) {
        self.runWOW()
        clearInterval(check)
      }
    });

    let check2 = setInterval(() => {
      if (window.$('.fancy-iframe-js').length) {
        self.runFancyBox()
        clearInterval(check2)
      }
    });
  }

  runWOW () {
    let wow = new window.WOW({
			boxClass:     'wow',      // default
			animateClass: 'animated', // default
			offset:       0,          // default
			mobile:       true,       // default
			live:         true        // default
		})
		wow.init()
  }

  runFancyBox () {
    window.$('.fancy-iframe-js').fancybox({
        padding: 0,
        margin: 10,
        wrapCSS: 'fancy-main'
    })
  }

  render() {
    return (
      <Router>
        <div className="main-wrapper">
          <Switch>
            <Route exact path='/' component={HomePage} />

            <Route exact path='/token' component={TokenPage} />

            <Route exact path="/platform" component={PlatformPage} />

            <Route exact path='/faq' component={FaqPage} />

            <Route exact path="/team" component={TeamPage} />

            <Route exact path='/privacy-privacy' component={PrivacyPolicyPage}
                   title={'Privacy Policy | Transcendence Network (TSD)'} />

            <Route exact path='/cookies-policy' component={CookiesPolicyPage}
                   title={'Cookies Policy | Transcendence Network (TSD)'} />

            <Route exact path='/disclaimers' component={DisclaimersPage}
                   title={'Disclaimer | Transcendence Network (TSD)'} />

            <Route exact path='/white-list' component={WhiteListPage}
                   title={'White List | Transcendence Network (TSD)'} />

            <Route exact path='/not-found' component={PageNotFound} />
            <Redirect from="/*" to='/not-found'/>

          </Switch>

          <Header />
          <Footer />
          <Cookie />
        </div>
      </Router>
    );
  }
}

export default App;
