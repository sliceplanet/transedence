import React, {Component} from 'react';
import NavItem from './NavItem';

class HeaderComponent extends Component{

    render(){
      const MainNav = [
          {
              itemName:'Home',
              itemLink: '/'
          },
          {
              itemName:'Token',
              itemLink: '/token'
          },
          {
              itemName:'Platform',
              itemLink: '/platform'
          },
          {
              itemName:'Team',
              itemLink: '/team'
          },
          {
              itemName:'FAQ',
              itemLink: '/faq'
          },
          {
              itemName:'Blog',
              itemLink: 'https://blog.tsd.network'
          },
          {
              itemName:'Resources',
              itemLink: '/#section-11'
          }
      ];

        let items = [];

        for (let i = 0; i < MainNav.length; i++){
            let item = MainNav[i];
            items.push(
                <NavItem
                    key={`nav-item${i}`}
                    index={i}
                    itemName={item.itemName}
                    itemLink={item.itemLink}
                />
            )
        }

        return(
            <ul className={'main-nav'}>
                {items}
            </ul>
        )
    }
}

export default HeaderComponent;
