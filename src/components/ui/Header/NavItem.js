import React, {Component} from 'react';
import { NavLink} from "react-router-dom";
import { HashLink } from 'react-router-hash-link';

class NavItem extends Component{

    constructor(){
        super();
        this.liItem = React.createRef();
    }

    render(){


        return(
            <li className="main-nav__item"
                ref={this.liItem}
            >
                {this.props.itemName === 'Blog' ?
                    <a href={this.props.itemLink}
                    className={'main-nav__link'}>
                        {this.props.itemName}
                    </a> :

                    this.props.itemName === 'Resources' ?
                        <HashLink to={this.props.itemLink}
                                  className="main-nav__link">
                            {this.props.itemName}
                        </HashLink> :

                        <NavLink to={this.props.itemLink}
                                 className="main-nav__link"
                                 activeStyle={{
                                   borderBottom: '5px solid #3B8BE7'
                                }}
                                 exact={true}>
                            {this.props.itemName}
                        </NavLink>
                }

            </li>
        )
    }

}

export default NavItem;
