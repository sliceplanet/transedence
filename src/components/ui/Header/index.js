import React, { Component } from 'react';
import { NavLink} from "react-router-dom";

import MainNav from './MainNav';

import Logo from '../../../assets/img/logo.svg'

class Header extends Component {
  componentDidMount () {
    let $ = window.$
    $(window).scroll(() => {
      if ($(window).scrollTop() > 1) {
          $('header').addClass("fixed");
      } else {
          $('header').removeClass("fixed")
      }
    })
  }

  render() {
    return (
      <header>
  			<div className="wrapper">
  				<div className="logo"><a href="#"><img src={Logo} alt="" /></a></div>
  				<div className="box-main-nav">
  					<a href="#" className={'button-nav'}>
  						<span></span>
  						<span></span>
  						<span></span>
  					</a>
  					<MainNav />
  				</div>
  			</div>
  		</header>
    );
  }
}

export default Header;
