import React, { Component } from 'react';

import FooterLogo from '../../assets/img/footer-logo.svg'

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
  			<div className="wrapper">
  				<div className="footer__wrap">
  					<div className="logo"><a href="/#/"><img src={FooterLogo} alt="" /></a></div>
  					<ul className="footer-nav">
  						<li className="footer-nav__item"><a href="/#/cookies-policy" className="footer-nav__link">Cookies Policy</a></li>
  						<li className="footer-nav__item"><a href="/#/disclaimers" className="footer-nav__link">Disclaimers</a></li>
  						<li className="footer-nav__item"><a href="/#/privacy-privacy" className="footer-nav__link">Privacy</a></li>
  						<li className="footer-nav__item"><a target="_blank" href="https://landing.tsd.network/contact-us" className="footer-nav__link">Contact us</a></li>
  					</ul>
  					<div className="social-list">
  						<div className="social-list__item"><a target="_blank" href="https://twitter.com/tsdtoken" className="social-list__link"><i className="icon-twitter-white"></i></a></div>
  						<div className="social-list__item"><a target="_blank" href="https://t.me/tsdtoken" className="social-list__link"><i className="icon-telegram"></i></a></div>
  						<div className="social-list__item"><a target="_blank" href="https://medium.com/@tsdnetwork" className="social-list__link"><i className="icon-medium-white"></i></a></div>
  						<div className="social-list__item"><a target="_blank" href="#" className="social-list__link"><i className="icon-bitcointalk-white"></i></a></div>
  						<div className="social-list__item"><a target="_blank" href="https://www.linkedin.com/company/tsd-network/" className="social-list__link"><i className="icon-linkedin-white"></i></a></div>
  						<div className="social-list__item"><a target="_blank" href="https://www.youtube.com/channel/UCdFcOLONAuF4-oN7buX4TvQ" className="social-list__link"><i className="icon-youtube-white"></i></a></div>
  						<div className="social-list__item"><a target="_blank" href="https://www.reddit.com/r/TSDToken/" className="social-list__link"><i className="icon-reddit-white"></i></a></div>
  						<div className="social-list__item"><a target="_blank" href="https://github.com/tsdtoken" className="social-list__link"><i className="icon-github-white"></i></a></div>
  					</div>
  				</div>
  			</div>
  		</footer>
    );
  }
}

export default Footer;
