import React, { Component } from 'react';

class Cookie extends Component {
  constructor (p) {
    super(p)

    this.state = { view: true }
  }
  handleClick (event) {
    this.setState({ view: false })
    window.localStorage.setItem('cookie', '1')
    window.$('body').removeClass('cookie')
    event.preventDefault()
  }

  render() {
    if(window.localStorage.getItem('cookie') && this.state.view === true) {
      this.setState({ view: false })
      window.$('body').removeClass('cookie')
    }
    return (
      <div className="box-cookie" style={{ display: this.state.view ? '': 'none' }}>
          <div className="box-cookie__text">
              This website uses cookies. By continuing to browse the site, you are agreeing to our <a href="cookies-policy.html">use of cookies.</a>
          </div>
          <a href="#" className="agree js-cookie-close" onClick={this.handleClick.bind(this)}>Agree</a>
      </div>
    );
  }
}

export default Cookie;
