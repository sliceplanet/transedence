import React, { Component } from 'react';
import { HashLink as Link } from 'react-router-hash-link';

class SideNav extends Component{
    constructor(){
        super();
        this.shadow = React.createRef();
    }
    //
    onBlurFunc(){
        let activeCl = 'active';
        let elCl = this.shadow.current.classList;
        elCl.contains(activeCl) ? elCl.remove(activeCl) : elCl.add(activeCl);
    }
    //
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(){
      let $ = window.$;
        let originalUrl = window.location.href,
            indexPart = originalUrl.split("#/#")[1],
            activeCl = 'active';
        let els = document.getElementsByClassName('sidebar-nav-list__item');
        if (indexPart !== undefined){

            for (let i = 0; i < els.length; i++){
                let el = els[i];
                el.classList.remove(activeCl);
                el.classList.contains(indexPart) && el.classList.add(activeCl);
            }
        }

        let item1 = $('div#introduction-video')
        let item2 = $('div#what-is-tsd')
        let item3 = $('div#our-platform')
        let item4 = $('div#our-track-record')
        let item5 = $('div#what-they-say-about-us')
        let item6 = $('div#our-roadmap')
        let item7 = $('div#how-were-different')
        let item8 = $('div#allocation-of-the-funds')
        let item9 = $('div#meet-the-team')
        let item10 = $('div#meet-the-partners')
        let item11 = $('div#resources')

        let clear = () => {
          $('.sidebar-nav-list__item.section-1').removeClass('active')
          $('.sidebar-nav-list__item.section-2').removeClass('active')
          $('.sidebar-nav-list__item.section-3').removeClass('active')
          $('.sidebar-nav-list__item.section-4').removeClass('active')
          $('.sidebar-nav-list__item.section-5').removeClass('active')
          $('.sidebar-nav-list__item.section-6').removeClass('active')
          $('.sidebar-nav-list__item.section-7').removeClass('active')
          $('.sidebar-nav-list__item.section-8').removeClass('active')
          $('.sidebar-nav-list__item.section-9').removeClass('active')
          $('.sidebar-nav-list__item.section-10').removeClass('active')
          $('.sidebar-nav-list__item.section-11').removeClass('active')
        }

        if (window.pageYOffset >= item1.offset().top && window.pageYOffset <= item2.offset().top) {
          clear()
          $('.sidebar-nav-list__item.section-1').addClass('active')

        } else if (window.pageYOffset+200 >= item2.offset().top && window.pageYOffset <= item3.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-2').addClass('active')

        } else if (window.pageYOffset+200 >= item3.offset().top && window.pageYOffset <= item4.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-3').addClass('active')

        } else if (window.pageYOffset+200 >= item4.offset().top && window.pageYOffset <= item5.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-4').addClass('active')

        } else if (window.pageYOffset+200 >= item5.offset().top && window.pageYOffset <= item6.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-5').addClass('active')

        } else if (window.pageYOffset+200 >= item6.offset().top && window.pageYOffset <= item7.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-6').addClass('active')

        } else if (window.pageYOffset+200 >= item7.offset().top && window.pageYOffset <= item8.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-7').addClass('active')

        } else if (window.pageYOffset+200 >= item8.offset().top && window.pageYOffset <= item9.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-8').addClass('active')

        } else if (window.pageYOffset+200 >= item9.offset().top && window.pageYOffset <= item10.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-9').addClass('active')

        } else if (window.pageYOffset+200 >= item10.offset().top && window.pageYOffset <= item11.offset().top-200) {
          clear()
          $('.sidebar-nav-list__item.section-10').addClass('active')

        } else if (window.pageYOffset >= item11.offset().top) {
          clear()
          $('.sidebar-nav-list__item.section-11').addClass('active')

        }
    };


    render(){
        return(
            <div>
                <div className="sidebar-nav-list__wrap"
                     onMouseOut={this.onBlurFunc.bind(this)}
                     onMouseOver={this.onBlurFunc.bind(this)}
                >
                    <ul className="sidebar-nav-list" id="js-sidebar-nav-list">
                        <li className="sidebar-nav-list__item section-1">
                            <Link smooth to="#introduction-video" className="sidebar-nav-list__link" >Introduction video</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-2">
                            <Link smooth to="#what-is-tsd" className="sidebar-nav-list__link" >What is TSD</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-3">
                            <Link smooth to="#our-platform" className="sidebar-nav-list__link" >Our platform</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-4">
                            <Link smooth to="#our-track-record" className="sidebar-nav-list__link" >Our track record</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-5">
                            <Link smooth to="#what-they-say-about-us" className="sidebar-nav-list__link" >What they say about us</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-6">
                            <Link smooth to="#our-roadmap" className="sidebar-nav-list__link" >Our roadmap</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-7">
                            <Link smooth to="#how-were-different" className="sidebar-nav-list__link" >How were different</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-8">
                            <Link smooth to="#allocation-of-the-funds" className="sidebar-nav-list__link" >Allocation of the funds</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-9">
                            <Link smooth to="#meet-the-team" className="sidebar-nav-list__link" >Meet the team</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-10">
                            <Link smooth to="#meet-the-partners" className="sidebar-nav-list__link" >Meet the partners</Link>
                        </li>
                        <li className="sidebar-nav-list__item section-11">
                            <Link smooth to="#resources" className="sidebar-nav-list__link" >Resources</Link>
                        </li>
                    </ul>
                </div>
                <div
                  className="overlay"
                  ref={this.shadow}
                  ></div>
            </div>
        )
    }
}

export default SideNav;
