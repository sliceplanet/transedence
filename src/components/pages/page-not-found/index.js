import React, { Component } from 'react';

class DisclaimersPage extends Component {
  componentDidMount () {
      document.title = '404 Page Not Found | Transcendence Network (TSD)'
      document.documentElement.scrollTop = 0;
  }

  render() {
    return (
      <main className="content">
  			<div className="box-error">
  				<div className="wrapper">
  					<div className="box-error__wrap">
  						<div className="box-error__title">404</div>
  						<div className="box-error__subtitle">
  							<strong>Page not found</strong><br />
  							We can’t seem to find the page you are looking for.
  						</div>
  						<a href="/#/" className="button">GO TO HOMEPAGE</a>
  					</div>
  				</div>
  			</div>
  		</main>
    );
  }
}

export default DisclaimersPage;
