import React, { Component } from 'react';

import PlatformMVPPoster from '../../../assets/img/video/PlatformMVP.png'
import PlatformMVPMp4 from '../../../assets/img/video/PlatformMVP.mp4'
import PlatformMVPPng from '../../../assets/img/video/PlatformMVP.png'

class Screen extends Component {
  render() {
    return (
      <section className="platform-mvp">
        <div className="wrapper">
          <div className="platform-mvp__wrap">
            <div className="platform-mvp__img">
              <video loop muted autoPlay poster={PlatformMVPPoster} className="main-header__video">
                  <source src={PlatformMVPMp4} type='video/mp4' className="sour1" />
                  <source src={PlatformMVPPng} type='video/webm' className="sour2" />
                    </video>
            </div>
                    <div className="platform-mvp__desc">
                      <div className="subtitle">A sneak peek at our work</div>
              <h2>Platform <span className="text-bg text-bg__small wow">MVP. </span></h2>
              <p>We have been very busy focusing on developing the Transcendence platform - concentrating on user research, interviews, validation, developing personas, user journeys and flows, sketching and initial wireframes.</p><p>We plan to regularly release new features to the MVP where it will then lead us to a first alpha release of the platform as detailed in our roadmap.</p>
                      <p>Currently, the Transcendence MVP demonstrates how a land owner, developer and investor may use the Transcendence market place.</p>
                      <a href="https://demo.tsd.network" className="button">GO TO MVP</a>
                  </div>
              </div>
        </div>
      </section>
    );
  }
}

export default Screen;
