import React, { Component } from 'react';

import MainScreen from './MainScreen.js'
import MvpScreen from './MvpScreen.js'
import ApplicationsScreen from './ApplicationsScreen.js'
import EcosystemScreen from './EcosystemScreen.js'
import FunctionalityScreen from './FunctionalityScreen.js'
import JourneyScreen from './JourneyScreen.js'
import Documents from '../../screens/DocumentsInfoScreen.js'
import WhiteList from '../../screens/WhiteListInfoScreen.js'

class PlatformPage extends Component {
  componentDidMount () {
      document.title = 'Our Platform | Transcendence Network (TSD)'
      document.documentElement.scrollTop = 0;
  }

  render() {
    return (
      <main className="content">
        <MainScreen />
        <MvpScreen />
        <ApplicationsScreen />
        <EcosystemScreen />
  			<FunctionalityScreen />
        <JourneyScreen />
        <Documents />
        <WhiteList />
  		</main>
    );
  }
}

export default PlatformPage;
