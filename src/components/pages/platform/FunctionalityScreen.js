import React, { Component } from 'react';

import BlockchainFunc from '../../../assets/img/blockchain-functionality.png'

class Screen extends Component {
  render() {
    return (
      <section className="platform-functionality">
        <div className="wrapper">
          <div className="platform-functionality__wrap">

            <div className="platform-functionality__img-wrap">
              <div className="platform-functionality__img">
                <img src={BlockchainFunc} alt="" />
              </div>
            </div>
            <div className="platform-functionality__desc">
              <div className="subtitle">How does it work?</div>
              <h2>Blockchain <span className="text-bg text-bg__small wow">functionality. </span></h2>
              <p>Transcendence is an overarching platform for the origination, vetting, financing and development of infrastructure projects using smart contracts, TSD Tokens and decentralized communities.</p>
            </div>
            <div className="journey-info__icon">
              <i className="icon-arr_slide_left"></i>
              <i className="icon-arr_slide_left right"></i>
            </div>
            <div className="journey-info__icon2">
              <i className="icon-hand"></i>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Screen;
