import React, { Component } from 'react';

import Energy from '../../../assets/img/Energy.gif'
import Farming from '../../../assets/img/Farming.gif'
import Transportation from '../../../assets/img/Transportation.gif'

class Screen extends Component {
  render() {
    return (
      <section className="platform-applications text-center">
        <div className="wrapper">
          <div className="platform-applications__text">
            <div className="subtitle">Platform features</div>
            <h2>A platform <span className="text-bg text-bg__small wow">for all</span> sustainability <br />projects.</h2>
            <p>Our core expertise is in renewables, but the Transcendence platform is build to support all types of sustainable infrastructure projects.</p>
          </div>
            <div className="platform-applications-list__wrap">
            <ul className="platform-applications-list">
              <li className="platform-applications-list__item">
                <div className="platform-applications-list__img">
                  <img src={Energy} alt="" />
                </div>
                <h3>Sustainable energy</h3>
                <p>Solar farm, Wind turbine, Hydroelectric power, Biomass and Geothermal based projects.</p>
              </li>
              <li className="platform-applications-list__item">
                <div className="platform-applications-list__img">
                  <img src={Farming} alt="" />
                </div>
                <h3>Sustainable agriculture</h3>
                <p>Permaculture, Biodynamics, Hydroponics and Urban agriculture projects.</p>
              </li>
              <li className="platform-applications-list__item">
                <div className="platform-applications-list__img">
                  <img src={Transportation} alt="" />
                </div>
                <h3>Sustainable transportation</h3>
                <p>Greenways, Foreshoreways, Bikeways, Busways and Railways projects.</p>
              </li>
            </ul>
          </div>
        </div>
      </section>
    );
  }
}

export default Screen;
