import React, { Component } from 'react';

import StackImage from '../../../assets/img/mvp-stack_1.png'

class Screen extends Component {
  render() {
    return (
      <section className="platform-main">
          <div className="wrapper">
              <div className="platform-main__box">
                  <div className="platform-main__info">
              <h1><span className="text-bg text-bg__big wow">A differentiated model </span><br />built on Blockchain <br/> Technology
                      </h1>
                      <p>
                         Transcendence is a platform to facilitate project development in a structured environment and connect multiple parties across the globe through the power of decentralized ledger applications and smart contract.
                      </p>
                      <a target="_blank" href="https://tsd.network/whitelist" className="button">JOIN THE WHITELIST</a>
                  </div>
                  <div className="platform-main__img">
                      <img src={StackImage} alt="" />
                  </div>
              </div>
          </div>
      </section>
    );
  }
}

export default Screen;
