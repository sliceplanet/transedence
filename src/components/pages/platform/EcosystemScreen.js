import React, { Component } from 'react';

import Diagram from '../../../assets/img/diagram.png'

class Screen extends Component {
  render() {
    return (
      <section className="platform-ecosystem">
        <div className="wrapper">
          <div className="platform-ecosystem__text">
            <div className="subtitle">How does it work?</div>
            <h2>The project <span className="text-bg text-bg__small wow">ecosystem.</span></h2>
            <p>The TSD Platform includes a nomber of modules designed to manage all aspects of the project development cycle. The diagram below shows the interactions of the key elements of the ecosystem and the platform modules. </p>
          </div>
          <div className="platform-ecosystem__img-wrap">
            <div className="platform-ecosystem__img">
              <img src={Diagram} alt="" />
            </div>
          </div>
          <div className="journey-info__icon">
            <i className="icon-arr_slide_left"></i>
            <i className="icon-arr_slide_left right"></i>
          </div>
          <div className="journey-info__icon2">
            <i className="icon-hand"></i>
          </div>
        </div>
      </section>
    );
  }
}

export default Screen;
