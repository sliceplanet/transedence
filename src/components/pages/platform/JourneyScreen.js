import React, { Component } from 'react';

class Screen extends Component {
  componentDidMount () {
    let self = this;

    let check = setInterval(() => {
      if (window.$('.js-platform-journey')) {
        self.runTimeLine()
        clearInterval(check)
      }
    })
  }

  runTimeLine () {
    let $ = window.$;
    let $frame = $('.js-platform-journey');
  	let $wrap  = $frame.parent();

		$frame.sly({
			horizontal: false,
		    itemNav: 'forceCentered',
			smart: 1,
			activateMiddle: true,
			activateOn: 'click',
			mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 9,
			scrollBar: $wrap.closest('.platform-journey__wrap').find('.scrollbar'),
			scrollBy: 1,
			speed: 500,
			elasticBounds: 1,
			easing: 'easeOutExpo',
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,
            pagesBar: $('.nav-pl-js'),
			activatePageOn: 'click',
			// Buttons
			prev: $wrap.find('.prev'),
			next: $wrap.find('.next'),


		}).init();
		$frame.sly('reload');
        $frame.sly('on', 'load move', function(){

       if($('.nav-pl-js li').eq(0).hasClass('active')
        || $('.nav-pl-js li').eq(1).hasClass('active')
        || $('.nav-pl-js li').eq(2).hasClass('active')
        || $('.nav-pl-js li').eq(3).hasClass('active')
        || $('.nav-pl-js li').eq(4).hasClass('active')){
         $('.platform-journey-year__item').removeClass('active')
         $('.platform-journey-year__item.item1').addClass('active')
        }
        if($('.nav-pl-js li').eq(5).hasClass('active')
        || $('.nav-pl-js li').eq(6).hasClass('active')){
         $('.platform-journey-year__item').removeClass('active')
         $('.platform-journey-year__item.item2').addClass('active')
        }
         if($('.nav-pl-js li').eq(7).hasClass('active')
       || $('.nav-pl-js li').eq(8).hasClass('active')
       || $('.nav-pl-js li').eq(9).hasClass('active')
       || $('.nav-pl-js li').eq(10).hasClass('active')){
         $('.platform-journey-year__item').removeClass('active')
         $('.platform-journey-year__item.item3').addClass('active')
        }
         if($('.nav-pl-js li').eq(11).hasClass('active')
       || $('.nav-pl-js li').eq(12).hasClass('active')
       || $('.nav-pl-js li').eq(13).hasClass('active')
       || $('.nav-pl-js li').eq(14).hasClass('active')){
         $('.platform-journey-year__item').removeClass('active')
         $('.platform-journey-year__item.item4').addClass('active')
        }
         if($('.nav-pl-js li').eq(15).hasClass('active')
        || $('.nav-pl-js li').eq(16).hasClass('active')
        || $('.nav-pl-js li').eq(17).hasClass('active')
        || $('.nav-pl-js li').eq(18).hasClass('active')){
         $('.platform-journey-year__item').removeClass('active')
         $('.platform-journey-year__item.item5').addClass('active')
        }
         if($('.nav-pl-js li').eq(19).hasClass('active')){
         $('.platform-journey-year__item').removeClass('active')
         $('.platform-journey-year__item.item6').addClass('active')
        }
         if($('.nav-pl-js li').eq(20).hasClass('active')){
         $('.platform-journey-year__item').removeClass('active')
         $('.platform-journey-year__item.item7').addClass('active')
        }



      $('.click-year2016').click(function(){

     $('.nav-pl-js li').eq(0).click()
     return false

     })

     $('.click-year2017').click(function(){

     $('.nav-pl-js li').eq(5).click()
     return false

     })
     $('.click-year2018').click(function(){

     $('.nav-pl-js li').eq(7).click()
     return false

     })
     $('.click-year2019').click(function(){

     $('.nav-pl-js li').eq(10).click()
     return false

     })
     $('.click-year2020').click(function(){

     $('.nav-pl-js li').eq(14).click()
     return false

     })
     $('.click-year2021').click(function(){

     $('.nav-pl-js li').eq(18).click()
     return false

     })
     $('.click-year2022').click(function(){

     $('.nav-pl-js li').eq(19).click()
     return false

     })

    })
  }

  render() {
    return (
      <section className="platform-journey text-center">
				<div id="our-roadmap" className="section-nav"></div>
				<div className="wrapper">
					<div className="platform-journey__info">
						<div className="subtitle">Our journey</div>
						<h2>Where we are, and <span className="text-bg text-bg__small wow">where we are going.</span></h2>
						<p>Our journey started many years ago, but with each milestone we are succesfully building to something more.</p>
					</div>
					<div className="platform-journey__wrap">
						<ul className="platform-journey-year pages-dots-js">
							<li className="platform-journey-year__item item1">
							    <a href="#" className="click-year2016">&gt; 2016</a>
							</li>
							<li className="platform-journey-year__item item2" >
							    <a href="#" className="click-year2017">2017</a>
							 </li>
							<li className="platform-journey-year__item item3 active">
							    <a href="#" className="click-year2018">2018</a>
							 </li>
							<li className="platform-journey-year__item item4">
							    <a href="#" className="click-year2019">2019</a>
							 </li>
							<li className="platform-journey-year__item item5">
							    <a href="#" className="click-year2020">2020</a>
							 </li>
							<li className="platform-journey-year__item item6">
							    <a href="#" className="click-year2021">2021</a>
							 </li>
							<li className="platform-journey-year__item item7">
							    <a href="#" className="click-year2022">2022 &gt;</a>
				            </li>
						</ul>
						<ul className="nav-pl-js"></ul>
						<div className="box-tab-cont">
							<div className="tab-cont" id="year-2016">
								<div className="frame platform-journey__slider-wrap js-platform-journey">
									<ul className="platform-journey__slider">
										<li className="platform-journey__item year2016" id="year2016">
											<div className="platform-journey__year">2009</div>
											<p>Retail Solar business was established and positioned as one of the first residential solar power retail companies in Australia. We sold and installed 300+ systems in NSW, Australia.</p>
										</li>
										<li className="platform-journey__item year2016">
											<div className="platform-journey__year">2011</div>
											<p>Wholesale business arm established, one of the largest wholesalers of solar power systems in Australia within 1st year of operation.</p>
										</li>
										<li className="platform-journey__item year2016">
											<div className="platform-journey__year">2013</div>
											<p>Won one of the first large scale solar FiT in Australia, Mugga Lane Solar Park (MLSP).</p>
										</li>
										<li className="platform-journey__item year2016">
											<div className="platform-journey__year">2015</div>
											<p>MLSP receives development approval from Australian government and enters Grid Connection Agreement.</p>
										</li>
										<li className="platform-journey__item year2016">
											<div className="platform-journey__year">2016</div>
											<p>Sunraysia Solar Farm (SSF) development commences to build the largest solar plant ever in Southern Hemisphere.</p>
											<p>MLSP is commissioned and connected to the National Electricity Market. </p>
										</li>
										<li className="platform-journey__item year2017" id='year2017'>
											<div className="platform-journey__year">Q3 2017</div>
											<p>SSF receives approval from Australia government and offer to connect from Transgrid</p>
										</li>
										<li className="platform-journey__item year2017">
											<div className="platform-journey__year">Q4 2017</div>
											<p>Blockchain arm Transcendence Network planning debuts</p>
											<p>Australia's largest solar deal is signed with the biggest energy retailer AGL.</p>
											<p>Australia's first corporate deal signed with UNSW and Origin Energy.</p>
										</li>
										<li className="platform-journey__item year2018"id='year2018'>
											<div className="platform-journey__year">Q1 2018</div>
											<p>Transcendence Network is established and TGE is planned.</p>
											<p>Establish token economics and business model.</p>
											<p>Founding team members and key advisors appointed.</p>
										</li>
										<li className="platform-journey__item year2018">
											<div className="platform-journey__year">Q2 2018</div>
											<p>Development of smart contracts; Private, Pre-sale and Main sale contracts.</p>
											<p>Transcendence platform strategy and roadmap conceptualisation.</p>
										</li>
										<li className="platform-journey__item year2018">
											<div className="platform-journey__year">Q3 2018</div>
											<p>Transcendence and infrastructure development roadmap blueprint created.</p>
											<p>Initial platform collaboration with advisors and partners to identify and scope opportunities for platform to solve real world issues.</p>
											<p>Completed smart contract and security audits.</p>
											<p>SSF achieves financial close</p>
										</li>
										<li className="platform-journey__item year2018">
											<div className="platform-journey__year">Q4 2018</div>
											<p>Deployment of smart contracts.</p>
											<p>Validation of key user journeys and build out of product backlog & roadmap.</p>
											<p>Begin system architecture development.</p>
											<p>MVP for Transcendence Platform released.</p>
											<p>Start alpha development of Transcendence Platform architecture and user on-boarding.</p>
										</li>
										<li className="platform-journey__item year2019" id="year2019">
											<div className="platform-journey__year">Q1 2019</div>
											<p>Private capital raised.</p>
											<p>Develop relationship with corporate partners.</p>
											<p>Token Sale Campaign. Public Pre-sale commence on March 15th.</p>
											<p>Alpha release of Transcendence Platform architecture and user on-boarding.</p>
											<p>Start alpha development for Transcendence Market Listing.</p>
										</li>
										<li className="platform-journey__item year2019">
											<div className="platform-journey__year">Q2 2019</div>
											<p>Establish Transcendence investment committee.</p>
											<p>Tokens are listed on public exchanges within 2 weeks after ICO finishes.</p>
											<p>Alpha Transcendence Market Listing release.</p>
											<p>Start Alpha Transcendence Market Place (surety bonding and project financing)</p>
										</li>
										<li className="platform-journey__item year2019">
											<div className="platform-journey__year">Q3 2019</div>
											<p>Beta release of Transcendence Market Place investment platform.</p>
											<p>First projects listed and funded.</p>
											<p>Start alpha development of Transcendence Platform token exchange mechanisms and community engagement module.</p>
											<p>SSF to achieve practical completion</p>
										</li>
										<li className="platform-journey__item year2019">
											<div className="platform-journey__year">Q3 2019</div>
											<p>Expand industry and corporate relationships.</p>
											<p>Start construction of funded projects.</p>
											<p>Alpha release of the Transcendence exchange module.</p>
										</li>
										<li className="platform-journey__item year2020" id="year2020">
											<div className="platform-journey__year">Q1 2020</div>
											<p>Transcendence marketing place investment and exchange modules fully functional.</p>
											<p>Alpha release of community engagement module.</p>
											<p>Start development of construction phase module.</p>
											<p>TSD Network's first community owned solar farm Midgar achieves financial close</p>
										</li>
										<li className="platform-journey__item year2020">
											<div className="platform-journey__year">Q2 2020</div>
											<p>Integration of community engagement module to the platform.</p>
											<p>Alpha release of construction phase module.</p>
										</li>
										<li className="platform-journey__item year2020">
											<div className="platform-journey__year">Q3 2020</div>
											<p>Construction completed on first projects.</p>
											<p>Start development of the Transcendence API Blockchain Interface.</p>
											<p>Start development of community voting.</p>
										</li>
										<li className="platform-journey__item year2020">
											<div className="platform-journey__year">Q4 2020</div>
											<p>Establish relationships with industry and corporate partners in other regions.</p>
											<p>Alpha release of the Transcendence API Blockchain interface.</p>
										</li>
										<li className="platform-journey__item year2021" id="year2021">
											<div className="platform-journey__year">2021</div>
											<p>Alpha release of Transcendence community voting.</p>
											<p>Midgar to achieve practical completion.</p>
										</li>
										<li className="platform-journey__item year2022" id="year2022">
											<div className="platform-journey__year">2022 &gt;</div>
											<p>TSD Network to have influence over 500MW of renewable energy generation</p>
										</li>
										<li className="platform-journey__item year2022">
											<div className="platform-journey__year">2023</div>
											<p>TSD Development Platform to support the first sustainable infrastructure development backed by EAs and community consensus</p>
										</li>
										<li className="platform-journey__item year2022">
											<div className="platform-journey__year">2024</div>
											<p>TSD Network to have influence over 1,000MW of renewable energy generation</p>
										</li>
									</ul>
								</div>
							</div>

						</div>
						<div className="scrollbar">

							<div className="handle">
								<div className="mousearea">
									<div className="btn backward"><i className="icon-arr_slide_left"></i></div>
								<div className="btn forward"><i className="icon-arr_slide_left right"></i></div>
								</div>
							</div>
						</div>
					</div>
					<div className="journey-info__icon">
						<i className="icon-arr_slide_left"></i>
						<i className="icon-arr_slide_left right"></i>
					</div>
					<div className="journey-info__icon2">
						<i className="icon-hand"></i>
					</div>
				</div>
			</section>
    );
  }
}

export default Screen;
