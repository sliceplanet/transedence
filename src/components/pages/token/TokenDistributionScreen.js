import React, { Component } from 'react';

class Screen extends Component {
  componentDidMount () {
    let self = this;

    let check = setInterval(() => {
      if (window.Highcharts && window.$('#graph1')) {
        self.runFirstHighcharts()
        clearInterval(check)
      }
    })

    let check2 = setInterval(() => {
      if (window.Highcharts && window.$('#graph2')) {
        self.runTwoHighcharts()
        clearInterval(check2)
      }
    })
  }

  runFirstHighcharts () {
    let Highcharts = window.Highcharts;

    var colors = Highcharts.getOptions().colors,
  		categories = [
  			"Private round",
  			"Pre-sale round",
  			"Public round",
  			"Founders, team and advisors",
  			"Bounty and rewards",
  			"Liquidity program",
  			"Project Implementation providers"
  		],
  		data = [
  			{
  				"y": 62.74,
  				"color": '#96BAE2',
  				"drilldown": {
  					"name": "Private round",
  					"categories": [
  						"Private round",
  					],
  					"data": [
  						40.0,
  					]
  				}
  			},
  			{
  				"y": 10.57,
  				"color": '#679CD2',
  				"drilldown": {
  					"name": "Pre-sale round",
  					"categories": [
  						"Pre-sale round",
  					],
  					"data": [
  						25,
  					]
  				}
  			},
  			{
  				"y": 7.23,
  				"color": '#4266B0',
  				"drilldown": {
  					"name": "Public round",
  					"categories": [
  						"Public round",
  					],
  					"data": [
  						15,
  					]
  				}
  			},
  			{
  				"y": 5.58,
  				"color": '#1C5FA6',
  				"drilldown": {
  					"name": "Founders, team and advisors",
  					"categories": [
  						"Founders, team and advisors",
  					],
  					"data": [
  						8,
  					]
  				}
  			},
  			{
  				"y": 4.02,
  				"color": '#153E69',
  				"drilldown": {
  					"name": "Bounty and rewards",
  					"categories": [
  						"Bounty and rewards",
  					],
  					"data": [
  						7,
  					]
  				}
  			},
  			{
  				"y": 1.92,
  				"color": '#0E2136',
  				"drilldown": {
  					"name": "Liquidity program",
  					"categories": [
  						"Liquidity program",
  					],
  					"data": [
  						3,
  					]
  				}
  			},
  			{
  				"y": 7.62,
  				"color": '#0E2136',
  				"drilldown": {
  					"name": 'Project Implementation providers',
  					"categories": [
  						'Project Implementation providers'
  					],
  					"data": [
  						2
  					]
  				}
  		}
  		],
  		browserData = [],
  		versionsData = [],
  		i,
  		j,
  		dataLen = data.length,
  		drillDataLen,
  		brightness;


  	// Build the data arrays
  	for (i = 0; i < dataLen; i += 1) {

  		// add browser data
  		browserData.push({
  			name: categories[i],
  			y: data[i].y,
  			color: data[i].color
  		});

  		// add version data
  		drillDataLen = data[i].drilldown.data.length;
  		for (j = 0; j < drillDataLen; j += 1) {
  			brightness = 0.2 - (j / drillDataLen) / 5;
  			versionsData.push({
  				name: data[i].drilldown.categories[j],
  				y: data[i].drilldown.data[j],
  				color: Highcharts.Color(data[i].color).brighten().get()
  			});
  		}
  	}

  	// Create the chart
		Highcharts.chart('graph1', {
			chart: {
				type: 'pie'
			},
			plotOptions: {
				pie: {
					shadow: false,
					center: ['50%', '50%']
				}
			},
			tooltip: {
				valueSuffix: '%'
			},
			series: [{
				name: 'Distribution',
				data: browserData,
				size: '60%',
				dataLabels: {
					formatter: function () {
						return this.y > 5 ? this.point.name : null;
					},
					color: '#ffffff',
					distance: -30
				}
			}, {
				name: 'Result',
				data: versionsData,
				size: '79%',
				innerSize: '79%',

				id: 'versions'
			}],
			responsive: {
				rules: [{
					condition: {
						maxWidth: 400
					},
					chartOptions: {
						series: [{
							id: 'versions',
							dataLabels: {
								enabled: false
							}
						}]
					}
				}]
			}
		});
  }

  runTwoHighcharts () {
    let Highcharts = window.Highcharts;
    
    var colors = Highcharts.getOptions().colors,
  		categories = [
  			"Project development",
  			"Platform development",
  			"Contingency",
  			"Offer, liquidity and listing costs",
  			"Business development",
  			"Business development"
  		],
  		data = [
  			{

  				"color": 'rgb(255,216,141)',
  				"drilldown": {
  					"name": "Project development",
  					"categories": [
  						"Project development",
  					],
  					"data": [
  						68.24,
  					]
  				}
  			},
  			{

  				"color": '#FECB66',
  				"drilldown": {
  					"name": "Contingency",
  					"categories": [
  						"Contingency",
  					],
  					"data": [
  						20.74,
  					]
  				}
  			},
  			{

  				"color": '#F3B246',
  				"drilldown": {
  					"name": "Offer, liquidity and listing costs",
  					"categories": [
  						"Offer, liquidity and listing costs",
  					],
  					"data": [
  						3.51,
  					]
  				}
  			},
  			{

  				"color": '#AE8336',
  				"drilldown": {
  					"name": "Platform development",
  					"categories": [
  						"Platform development",
  					],
  					"data": [
  						2.54,
  					]
  				}
  			},
  			{

  				"color": '#705525',
  				"drilldown": {
  					"name": "Business development + Marketing",
  					"categories": [
  						"Business development + Marketing",
  					],
  					"data": [
  						2.48,
  					]
  				}
  			},
  			{

  				"color": '#40321A',
  				"drilldown": {
  					"name": "Operational costs and overheads",
  					"categories": [
  						"Operational costs and overheads",
  					],
  					"data": [
  						2.48,
  					]
  				}
  			}
  		],
  		browserData = [],
  		versionsData = [],
  		i,
  		j,
  		dataLen = data.length,
  		drillDataLen,
  		brightness;


  	// Build the data arrays
  	for (i = 0; i < dataLen; i += 1) {

  		// add browser data
  		browserData.push({
  			name: categories[i],
  			y: data[i].y,
  			color: data[i].color
  		});

  		// add version data
  		drillDataLen = data[i].drilldown.data.length;
  		for (j = 0; j < drillDataLen; j += 1) {
  			brightness = 0.2 - (j / drillDataLen) / 5;
  			versionsData.push({
  				name: data[i].drilldown.categories[j],
  				y: data[i].drilldown.data[j],
  				color: Highcharts.Color(data[i].color).brighten().get()
  			});
  		}
  	}
  	// Create the chart
		Highcharts.chart('graph2', {
			chart: {
				type: 'pie'
			},

			plotOptions: {
				pie: {
					shadow: false,
					center: ['50%', '50%']
				}
			},
			tooltip: {
				valueSuffix: '%'
			},
			series: [{
				name: 'Distribution',
				data: browserData,
				size: '60%',
				dataLabels: {
					formatter: function () {
						return this.y > 5 ? this.point.name : null;
					},
					distance: -30
				}
			}, {
				name: 'Result',
				data: versionsData,
				size: '78%',
				innerSize: '78%',

				id: 'versions'
			}],
			responsive: {
				rules: [{
					condition: {
						maxWidth: 400
					},
					chartOptions: {
						series: [{
							id: 'versions',
							dataLabels: {
								enabled: false
							}
						}]
					}
				}]
			}
		});
  }

  render() {
    return (
      <section className="token-distribution">
        <div className="wrapper">
          <div className="token-distribution__box">
            <div className="subtitle">
              How are tokens distributed?
            </div>
            <h2>
              Breaking down the token <br />
              <span className="text-bg text-bg__small wow ">
                distribution.&nbsp;
              </span>
            </h2>
            <div className="token-distribution__statistics">
              <ul className="token-distribution__list">
                <li className="token-distribution__item item1">
                  <span className="token-distribution__name">
                    Private round
                  </span>
                  <span className="token-distribution__num">
                    40.00%
                  </span>
                </li>
                <li className="token-distribution__item item2">
                  <span className="token-distribution__name">
                    Pre-sale round
                  </span>
                  <span className="token-distribution__num">
                    25.00%
                  </span>
                </li>
                <li className="token-distribution__item item3">
                  <span className="token-distribution__name">
                    Public round
                  </span>
                  <span className="token-distribution__num">
                    15.00%
                  </span>
                </li>
                <li className="token-distribution__item item4">
                  <span className="token-distribution__name">
                    Founders, team and advisors
                  </span>
                  <span className="token-distribution__num">
                    8.00%
                  </span>
                </li>
                <li className="token-distribution__item item5">
                  <span className="token-distribution__name">
                    Bounty and rewards
                  </span>
                  <span className="token-distribution__num">
                    7.00%
                  </span>
                </li>
                <li className="token-distribution__item item6">
                  <span className="token-distribution__name">
                    Liquidity program
                  </span>
                  <span className="token-distribution__num">
                    3.00%
                  </span>
                </li>
                <li className="token-distribution__item item7">
                  <span className="token-distribution__name">
                    Project Implementation providers
                  </span>
                  <span className="token-distribution__num">
                    2.00%
                  </span>
                </li>
              </ul>
              <div className="token-distribution__graph graph1" id="graph1"></div>
            </div>
            <div className="token-distribution__statistics stat2">
              <ul className="token-distribution__list">
                <li className="token-distribution__item item8">
                  <span className="token-distribution__name">
                    Project development
                  </span>
                  <span className="token-distribution__num">
                    68.24%
                  </span>
                </li>
                <li className="token-distribution__item item10">
                  <span className="token-distribution__name">
                    Contingency
                  </span>
                  <span className="token-distribution__num">
                    20.74%
                  </span>
                </li>
                <li className="token-distribution__item item11">
                  <span className="token-distribution__name">
                    Offer, liquidity and listing costs
                  </span>
                  <span className="token-distribution__num">
                    3.51%
                  </span>
                </li>
                <li className="token-distribution__item item9">
                  <span className="token-distribution__name">
                    Platform development
                  </span>
                  <span className="token-distribution__num">
                    2.54%
                  </span>
                </li>
                <li className="token-distribution__item item12">
                  <span className="token-distribution__name">
                    Business development + Marketing
                  </span>
                  <span className="token-distribution__num">
                    2.48%
                  </span>
                </li>
                <li className="token-distribution__item item13">
                  <span className="token-distribution__name">
                    Operational costs and overheads
                  </span>
                  <span className="token-distribution__num">
                    2.48%
                  </span>
                </li>

              </ul>
              <div className="token-distribution__graph graph2" id="graph2">

              </div>

            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Screen;
