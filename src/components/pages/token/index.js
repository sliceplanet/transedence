import React, { Component } from 'react';

import TokenMainScreen from './TokenMainScreen.js'
import TokenOfferingScreen from './TokenOfferingScreen.js'
import TokenDistributionScreen from './TokenDistributionScreen.js'
import Documents from '../../screens/DocumentsInfoScreen.js'
import WhiteList from '../../screens/WhiteListInfoScreen.js'

class TokenPage extends Component {
  componentDidMount () {
      document.title = 'Token Offering | Transcendence Network (TSD)'
      document.documentElement.scrollTop = 0;
  }

  render() {
    return (
      <main className="content">
        <TokenMainScreen />
        <TokenOfferingScreen />
        <TokenDistributionScreen />
        <Documents />
        <WhiteList />
  		</main>
    );
  }
}

export default TokenPage;
