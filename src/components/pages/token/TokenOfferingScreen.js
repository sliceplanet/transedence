import React, { Component } from 'react';

class Screen extends Component {
  render() {
    return (
      <section className="token-offering orange">
        <div className="token-offering__circle">

        </div>
        <div className="wrapper">
          <div className="subtitle">TSD token generation event</div>
          <h2>Our <span className="text-bg text-bg__small wow">token offering.&nbsp;</span></h2>
          <p>
            Transcendence is creating a total supply of up to approximately 250 million TSD tokens with up to 200 million of those tokens available during the Inital Token Offering.
          </p>
          <div className="token-offering__wrap">
            <div className="token-offering__wrap-inside">
              <div className="token-offering__box">
                <table className="token-offering__table">
                  <tr className="token-offering__tr">
                    <th className="token-offering__th">
                      Round
                    </th>
                    <th className="token-offering__th">Discount</th>
                    <th className="token-offering__th">1 Token (USD)</th>
                  </tr>
                  <tr className="token-offering__tr">
                    <td className="token-offering__td">
                      <span className="token-offering__marker marker1"></span>
                      Private
                    </td>
                    <td className="token-offering__td">30%</td>
                    <td className="token-offering__td">$0.35</td>
                  </tr>
                  <tr className="token-offering__tr">
                    <td className="token-offering__td">
                      <span className="token-offering__marker marker2"></span>
                      Pro Sale
                    </td>
                    <td className="token-offering__td">20%</td>
                    <td className="token-offering__td">$0.40</td>
                  </tr>
                  <tr className="token-offering__tr">
                    <td className="token-offering__td">
                      <span className="token-offering__marker marker3"></span>
                      Main
                    </td>
                    <td className="token-offering__td">No discount</td>
                    <td className="token-offering__td">$0.50</td>
                  </tr>
                </table>
                <div className="token-offering__progress">
                  <div className="token-offering__item">
                    <div className="token-offering__title">
                      Private round
                    </div>
                    <div className="token-offering__range range1">

                    </div>
                  </div>
                  <div className="token-offering__item">
                    <div className="token-offering__title">
                      Pre round
                    </div>
                    <div className="token-offering__range range2">

                    </div>
                  </div>
                  <div className="token-offering__item">
                    <div className="token-offering__title">
                      Main round
                    </div>
                    <div className="token-offering__range range3">

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Screen;
