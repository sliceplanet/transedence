import React, { Component } from 'react';

import PayImg from '../../../assets/img/pay.png'

class Screen extends Component {
  render() {
    return (
      <section className="token-main">
        <div className="wrapper">
          <div className="token-main__box">
            <div className="token-main__left">
              <h1>
                A currency owned by the&nbsp;
                <span className="text-bg text-bg__big wow">
                  community.&nbsp;
                </span>
              </h1>
              <p>
                Transcendence is a blockchain-based platform for the development and management of large scale sustainable and socially valuable infrastructure projects.
              </p>
              <a target="_blank" href="https://tsd.network/whitelist" className="button">JOIN THE WHITELIST</a>
            </div>
            <div className="token-main__right">
              <img src={PayImg} alt="" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Screen;
