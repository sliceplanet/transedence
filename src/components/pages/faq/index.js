import React, { Component } from 'react';

import Documents from '../../screens/DocumentsInfoScreen.js'
import WhiteList from '../../screens/WhiteListInfoScreen.js'
class FaqPage extends Component {
  componentDidMount () {
      document.title = 'FAQ | Transcendence Network (TSD)'
      document.documentElement.scrollTop = 0;
  }

  render() {
    return (
      <main className="content cookies">
  		    <section className="cookies-main">
  		        <div className="wrapper">
  		            <h1>Frequently asked questions</h1>
  		           	<p>Our intention is always to be clear and transparent in every aspect of our business.<br />
  					Here are questions raised by companies, communities and ourselves that we feel are important to acknowledge.</p>
  				<div className="faqrow">
  					<div className="faqcolumn">
  						<div className="faqincolumn">
  						<h3>What is Transcendence?</h3>
  						<p>- Transcendence is a blockchain-based, sustainable infrastructure development and collaboration platform.<br />
  						- We seek to accelerate the advent of the Sustainability Revolution by empowering communities and simplifying infrastructure development. <br />
  						- Transcendence draws upon the combined knowledge, skill base and funding capacity of token holders to develop sustainable infrastructure assets.<br />
  						- We are about creating relationships and bonds between token holders and participants of infrastructure development by way of rewarding and acknowledging without discrimination.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>What background does your team have?</h3>
  						<p>The Transcendence team has extensive experience in renewable infrastructure development across China, Australia and Asia Pacific, having over 30 years’ collective experience between the team members.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Will there be future issuances of tokens?</h3>
  						<p>As new projects reach the final stages on the TSD Platform, projects will be funded through a range of traditional debt, equity, hybrid instruments and token issuances. TSD will issue new TSD Tokens to support future project initiatives as they reach the financing phase.</p>
  						<p>Future issuances will be carefully managed to avoid dilution and be based on adding value to the system overall.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Where is the company registered?</h3>
  						<p>Singapore.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>What is the bonus?</h3>
  						<p>There is no bonus, only a discount which depends on the stage you participate in.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Is the company or the TGE audited?</h3>
  						<p>The TGE and smart contracts have been audited by IOSIRO and Icorating. You'll be able to review their audit via our Github.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>What is the minimum and maximum buy in for private pre-sale?</h3>
  						<p>$50,000 minimum and $10,000,000 maximum.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>What is the price of the token?</h3>
  						<p>Price ranges from $0.35 in the private pre-sale up to $0.50 in the public main sale.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>What is the hard cap and soft cap?</h3>
  						<p>The hard cap is $80.6 million and the soft cap is $20 million.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>How many stages are there in the token sale?</h3>
  						<p>There are three stages, which are Private Pre-sale, Public Pre-sale, and Public Main Sale.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>What is the allocation for bounty?</h3>
  						<p>7% allocated for the bounty program.</p>
  						</div>
  					</div>
  					<div className="faqcolumn">
  						<div className="faqincolumn">
  						<h3>Social Rewards AirDrop Campaign</h3>
  						<p>The TSD team believes in rewarding the loyalty of TSD token holders and plans to run a Social Rewards AirDrop Campaign on an semi-annual basis for the benefit of the TSD community. The features of the Social Rewards AirDrop Campaign include:<br />
  						- Operation of an AirDrop approximately every six months (no set date); <br />
  						- Benefits provided via the TSD IFR tokens which may transmute to additional primary TSD Tokens; <br />
  						- Supply of rewards to be allocated may be up to 5% of the total project net economic benefit to the token economy; <br />
  						- All primary token holders at the time of are eligible for rewards<br />
  						Further to the AirDrop Campaign, the TSD team is currently discussing other activities that aims to offer benefits to the TSD platform and community. </p>
  						</div>
  						<div className="faqincolumn">
  						<h3>How does the token work?</h3>
  						<p>- Access to the TSD Platform and ecosystem,<br />
  						- Access to Social Awards associated with proceeds from projects,<br />
  						- Priority allocation for future asset tokenizations,<br />
  						- Available to be held for long-term benefit or traded,<br />
  						- Can be traded on exchanges or via the TSD Liquidity Program.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>How does the token work?</h3>
  						<p>Telegram: http://t.me/tsdtoken<br />
  						Twitter: https://twitter.com/tsdtoken<br />
  						LinkedIn: https://www.linkedin.com/company/tsd-network/<br />
  						Medium: https://medium.com/@tsdnetwork<br />
  						Reddit: https://www.reddit.com/r/TSDToken/<br />
  						Github: https://github.com/tsdtoken</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Is Trancensdence a dual token system?</h3>
  						<p>The TSD Token economy is a two tier token economy with a primary token (TSD Token) smart contract and an internal platform token (IFR Token).</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Which countries are restricted from the sale?</h3>
  						<p>Residents from the US (unless you're an accredited investor), Singapore, and China may not participate in the token sale. The offer is not available to people physically located in Australia.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Can US residents participate if they're an accredited investor?</h3>
  						<p>Yes, you have to be an accredited investor to participate.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Do non-US citizens need to be accredited?</h3>
  						<p>No.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>What will happen to unsold tokens?</h3>
  						<p>Unsold tokens will be burned.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Accepted currencies during the sales?</h3>
  						<p>ETH, and also select fiat currencies. However, only ETH will be accepted during the public main sale.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>Is there an escrow period for private pre-sale tokens?</h3>
  						<p>Yes, escrow period is 9 months after conclusion of the main token sale.</p>
  						</div>
  						<div className="faqincolumn">
  						<h3>There is a lockup for bounty tokens?</h3>
  						<p>Yes, there will be a lockup of 6 months for bounty tokens.</p>
  						</div>
  					</div>
  				</div>
  		        </div>
  		        <div className="cookies-bg"></div>
  		    </section>
          <Documents />
          <WhiteList />
  		</main>
    );
  }
}

export default FaqPage;
