import React, { Component } from 'react';

class WhiteListPage extends Component {
  componentDidMount () {
      document.title = 'White List | Transcendence Network (TSD)'
      document.documentElement.scrollTop = 0;
  }

  render() {
    return (
      <main className="content">
  			<div className="box-error">
  				<div className="wrapper">
  					<div className="box-error__wrap">
  						{/* <div className="box-error__title">THE WHITELIST WILL OPEN SHORTLY</div> */}
  						<div className="box-error__subtitle">
  							<span style="font-size: 30px; color: #000;"><strong>THE WHITELIST WILL OPEN SHORTLY</strong></span> <br />
  							Please check back later
  						</div>
  						<a href="/#/" className="button">GO TO HOMEPAGE</a>
  					</div>
  				</div>
  			</div>
  		</main>
    );
  }
}

export default WhiteListPage;
