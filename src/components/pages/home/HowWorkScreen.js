import React, { Component } from 'react';



class MainScreen extends Component {
  render() {
    return (
      <section className="how-work text-center orange">
        <div className="wrapper">
          <div id="our-platform" className="section-nav"></div>
          <div className="how-work__text">
            <div className="subtitle">How does Transcendence work?</div>
            <h2>Everyone has an opportunity to contribute <span className="text-bg text-bg__small wow">their own way.&nbsp;</span></h2>
            <p>Transcendence is responding to the strong demand and support for efficient, reliable , renewable energy projects, offering a financing model that allows everyone to share in the benefits.</p>
            <p>We are a clear alternative to investment banks with cost effective token capital structure and an open, transparent model.</p>
          </div>
          <ul className="how-work-list">
            <li className="how-work-list__item">
              <div className="how-work-list__cont">
                <div className="how-work-list__icon"><i className="icon-icon_1"></i></div>
                <h3>Project idea and scope</h3>
                <p>Land listing and government approvals.</p>
              </div>
            </li>
            <li className="how-work-list__item">
              <div className="how-work-list__cont">
                <div className="how-work-list__icon"><i className="icon-icon_2"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span></i></div>
                <h3>Funding with TSD tokens</h3>
                <p>Any token holder can make <br /> investments.</p>
              </div>
            </li>
            <li className="how-work-list__item">
              <div className="how-work-list__cont">
                <div className="how-work-list__icon"><i className="icon-icon_3"></i></div>
                <h3>Building with renewable energy</h3>
                <p>Voting on decisions from community stakeholders</p>
              </div>
            </li>
            <li className="how-work-list__item">
              <div className="how-work-list__cont">
                <div className="how-work-list__icon"><i className="icon-icon_4"></i></div>
                <h3>Operating on our platform</h3>
                <p>All parties have complete transparency in future agreements</p>
              </div>
            </li>
            <li className="how-work-list__item">
              <div className="how-work-list__cont">
                <div className="how-work-list__icon"><i className="icon-icon_5"></i></div>
                <h3>Value and profits shared to token holders</h3>
                <p>TSD token raises in market <br /> from infrastructure value <br /> increase</p>
              </div>
            </li>
          </ul>
        </div>
      </section>
    );
  }
}

export default MainScreen;
