import React, { Component } from 'react';

import Founds from '../../../assets/img/funds-raised.png'

class MainScreen extends Component {
  render() {
    return (
      <section className="allocation-info">
        <div id="allocation-of-the-funds" className="section-nav"></div>
        <div className="wrapper">
          <div className="allocation-info__text">
            <div className="subtitle">Allocation of the funds</div>
            <h2>We believe in <br /><span className="text-bg text-bg__small wow">transparency.</span></h2>
            <p>With our Soft Cap set at $20 million, we have forecasted this to cover the necessary costs needed to carry out the continual development and refining of the platform, ongoing operation and most importantly, the seeding of the first real world operational asset into the platform, providing the token with real world underlying value.</p>
            <p>Any funds raised on top the softcap will be used to accelerate the development of the platform, further development of new projects and seeding the platform with more real world operational assets.</p>
            <p><i>*Visualisation; figures are not final</i></p>

          </div>
          <div className="allocation-info__img">
            <img src={Founds} alt="" />
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
