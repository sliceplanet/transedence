import React, { Component } from 'react';

import nasdaqSvg from '../../../assets/img/nasdaq.svg'
import reutersSvg from '../../../assets/img/reuters.svg'
import ifengSvg from '../../../assets/img/ifeng.svg'
import sinaSvg from '../../../assets/img/sina.svg'

class MainScreen extends Component {
  componentDidMount () {
    let self = this;

    let check = setInterval(() => {
      if (window.$('.transcendence-info__box-js').slick) {
        self.runSlick()
        clearInterval(check)
      }
    });
  }

  runSlick () {
    window.$('.transcendence-info__box-js').slick({
			dots: true,
			infinite: true,
			speed: 500,
            fade:true,
			slidesToShow: 1,
			dots: false,
			responsive: [
				{
				  breakpoint: 768,
				  settings: {

					arrows: false,
					dots: true
				  }
				}

			]
		})

    if(window.$('.transcendence-info__box-js .slick-arrow').length) {
  		window.$('.transcendence-info__box-js .slick-arrow').text('');
  	}
  }

  render() {
    return (
      <div className="transcendence-info">
        <div id="what-they-say-about-us" className="section-nav"></div>
        <div className="wrapper">
          <div className="transcendence-info__wrapper">
            <div className="transcendence-info__box transcendence-info__box-js">
              <div className="transcendence-info__item">
                <div className="transcendence-info__comment">
                  <i className="icon-quote"></i>
                  <div className="transcendence-info__text">
                    Transcendence is innovative and smart - with a mission to unlock the benefits of renewable energy for communities everywhere.
                  </div>
                  <a href="https://www.news.com.au/" className="transcendence-info__link">
                    - SIMON CORBEL, FORMER DEPUTY CHIEF MINISTER
                  </a>
                </div>
              </div>
              <div className="transcendence-info__item">
                <div className="transcendence-info__comment">
                  <i className="icon-quote"></i>
                  <div className="transcendence-info__text">
                    Transcendence is innovative and smart - with a mission to unlock the benefits of renewable energy for communities everywhere.
                  </div>
                  <a href="https://www.news.com.au/" className="transcendence-info__link">
                    - SIMON CORBEL, FORMER DEPUTY CHIEF MINISTER
                  </a>
                </div>
              </div>
              <div className="transcendence-info__item">
                <div className="transcendence-info__comment">
                  <i className="icon-quote"></i>
                  <div className="transcendence-info__text">
                    Transcendence is innovative and smart - with a mission to unlock the benefits of renewable energy for communities everywhere.
                  </div>
                  <a href="https://www.news.com.au/" className="transcendence-info__link">
                    - SIMON CORBEL, FORMER DEPUTY CHIEF MINISTER
                  </a>
                </div>
              </div>
              <div className="transcendence-info__item">
                <div className="transcendence-info__comment">
                  <i className="icon-quote"></i>
                  <div className="transcendence-info__text">
                    Transcendence is innovative and smart - with a mission to unlock the benefits of renewable energy for communities everywhere.
                  </div>
                  <a href="https://www.news.com.au/" className="transcendence-info__link">
                    - SIMON CORBEL, FORMER DEPUTY CHIEF MINISTER
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="transcendence-info__block">
            <ul className="transcendence-info__list">
              <li className="transcendence-info__element">
                <div className="transcendence-info__img">
                  <img src={nasdaqSvg} alt="" />
                </div>
              </li>
              <li className="transcendence-info__element">
                <div className="transcendence-info__img">
                  <img src={reutersSvg} alt="" />
                </div>
              </li>
              <li className="transcendence-info__element">
                <div className="transcendence-info__img">
                  <img src={ifengSvg} alt="" />
                </div>
              </li>
              <li className="transcendence-info__element">
                <div className="transcendence-info__img">
                  <img src={sinaSvg} alt="" />
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default MainScreen;
