import React, { Component } from 'react';

import tsd from '../../../assets/img/tsd-icon.svg'
import powerledger from '../../../assets/img/powerledger-icon.svg'
import wepower from '../../../assets/img/wepower-icon.svg'
import electrifyasia from '../../../assets/img/electrifyasia-icon.svg'

class MainScreen extends Component {
  render() {
    return (
      <section className="compare-info">
        <div id="how-were-different" className="section-nav"></div>
        <div className="wrapper">
          <div className="compare-info__text">
            <div className="subtitle">How were different</div>
            <h2>A <span className="text-bg text-bg__small wow">differentiation</span> model.</h2>
            <p>Transcendence delivers a differentiated model compared to current competitors in the blockchain space. Transcendence offers a model that provides solutions for entrepreneurs and sustainable energy developers along the infrastructure development value chain.</p>
          </div>
          <div className="compare-info-list__wrap">
              <div className="compare-info-list-block">
                            <ul className="compare-info-list">
                                <li className="compare-info-list__item compare-info-list__header">
                                    <div className="col col1"></div>
                                    <div className="col col2">
                                        <div className="compare-info-list__img">
                                            <img src={tsd} alt="" />
                                        </div>
                                        <div className="compare-info-list__title">Transcendence</div>
                                        <div className="compare-info-list__subtitle">TSD</div>
                                    </div>
                                    <div className="col col3">
                                        <div className="compare-info-list__img">
                                            <img src={powerledger} alt="" />
                                        </div>
                                        <div className="compare-info-list__title">Powerledger</div>
                                        <div className="compare-info-list__subtitle">POWR</div>
                                    </div>
                                    <div className="col col4">
                                        <div className="compare-info-list__img">
                                            <img src={wepower} alt="" />
                                        </div>
                                        <div className="compare-info-list__title">WePower</div>
                                        <div className="compare-info-list__subtitle">WPR</div>
                                    </div>
                                    <div className="col col5">
                                        <div className="compare-info-list__img">
                                            <img src={electrifyasia} alt="" />
                                        </div>
                                        <div className="compare-info-list__title">Electrifys</div>
                                        <div className="compare-info-list__subtitle">ELEC</div>
                                    </div>
                                </li>
                                <li className="compare-info-list__item">
                                    <div className="col col1">Origination</div>
                                    <div className="col col2"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col3"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col4"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col5"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                </li>
                                <li className="compare-info-list__item">
                                    <div className="col col1">Planning</div>
                                    <div className="col col2"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col3"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col4"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col5"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                </li>
                                <li className="compare-info-list__item">
                                    <div className="col col1">Funding</div>
                                    <div className="col col2"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col3"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col4"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col5"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                </li>
                                <li className="compare-info-list__item">
                                    <div className="col col1">Construction</div>
                                    <div className="col col2"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col3"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col4"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col5"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                </li>
                                <li className="compare-info-list__item">
                                    <div className="col col1">Operations</div>
                                    <div className="col col2"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col3"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col4"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col5"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                </li>
                                <li className="compare-info-list__item">
                                    <div className="col col1">Market operations</div>
                                    <div className="col col2"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col3"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col4"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col5"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                </li>
                                <li className="compare-info-list__item">
                                    <div className="col col1">End of Life</div>
                                    <div className="col col2"><div className="compare-info-list__icon active"><i className="icon-check"></i></div></div>
                                    <div className="col col3"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col4"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                    <div className="col col5"><div className="compare-info-list__icon"><i className="icon-check"></i></div></div>
                                </li>
                            </ul>
                        </div>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
