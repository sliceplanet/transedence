import React, { Component } from 'react';

import Video1HeaderPoster from '../../../assets/img/Header.png'
import Video1HeaderMp4 from '../../../assets/img/video/Header.mp4'
import Video1HeaderWebm from '../../../assets/img/video/Header.webm'

class MainScreen extends Component {
  render() {
    return (
      <section className="main-screen">
        <div id="section-1" className="section-nav"></div>

        <video loop muted autoPlay poster={Video1HeaderPoster} className="main-header__video">
          <source src={Video1HeaderMp4} type='video/mp4' className="sour1" />
          <source src={Video1HeaderWebm} type='video/webm' className="sour2" />
        </video>

        <div className="wrapper">
          <div className="main-screen__text">
            <h1>A community platform <br /> to create <br /><span className="text-bg text-bg__big wow"> sustainable&nbsp;</span> infrastructure.</h1>
            <p>Transcendence is a blockchain-based platform for the development and management of large scale sustainable and socially valuable infrastructure projects.</p>
            <div className="box-button box-button_main-screen">
              <a target="_blank" href="https://tsd.network/whitelist" className="button">JOIN THE WHITELIST</a>

            </div>
            <div className="social-list">
              <div className="social-list__item"><a target="_blank" href="https://twitter.com/tsdtoken" className="social-list__link"><i className="icon-twitter-white"></i></a></div>
              <div className="social-list__item"><a target="_blank" href="https://t.me/tsdtoken" className="social-list__link"><i className="icon-telegram"></i></a></div>
              <div className="social-list__item"><a target="_blank" href="https://medium.com/@tsdnetwork" className="social-list__link"><i className="icon-medium-white"></i></a></div>
              <div className="social-list__item"><a target="_blank" href="#" className="social-list__link"><i className="icon-bitcointalk-white"></i></a></div>
              <div className="social-list__item"><a target="_blank" href="https://www.linkedin.com/company/tsd-network/" className="social-list__link"><i className="icon-linkedin-white"></i></a></div>
              <div className="social-list__item"><a target="_blank" href="https://www.youtube.com/channel/UCdFcOLONAuF4-oN7buX4TvQ" className="social-list__link"><i className="icon-youtube-white"></i></a></div>
              <div className="social-list__item"><a target="_blank" href="https://www.reddit.com/r/TSDToken/" className="social-list__link"><i className="icon-reddit-white"></i></a></div>
              <div className="social-list__item"><a target="_blank" href="https://github.com/tsdtoken" className="social-list__link"><i className="icon-github-white"></i></a></div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
