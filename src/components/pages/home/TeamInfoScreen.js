import React, { Component } from 'react';

import ImgPoster from '../../../assets/img/img_video_large.jpg'

class MainScreen extends Component {
  render() {
    return (
      <section className="team-info text-center">
        <div id="meet-the-team" className="section-nav"></div>
        <div className="wrapper">
          <div className="team-info__text">
            <div className="subtitle">Our people</div>
            <h2>Meet the team.</h2>
            <p>Let us walk you through our vision for the Transcendence platform.</p>
            <div className="video">
              <a href="https://www.youtube.com/embed/WZJwacd43yE?autoplay=1" className="video__link fancy-iframe-js" data-fancybox-type="iframe">
                <img src={ImgPoster} alt="" />
                <span className="play-video">
                  <span className="play-video__button">
                    PLAY VIDEO
                  </span>
                </span>
              </a>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
