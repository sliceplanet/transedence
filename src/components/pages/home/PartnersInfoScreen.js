import React, { Component } from 'react';

import PartnersImg1 from '../../../assets/img/partners-img_1.svg'
import PartnersImg2 from '../../../assets/img/partners-img_2.svg'
import PartnersImg3 from '../../../assets/img/partners-img_3.svg'
import PartnersImg4 from '../../../assets/img/partners-img_4.svg'
import PartnersImg5 from '../../../assets/img/partners-img_5.svg'
import PartnersImg6 from '../../../assets/img/partners-img_6.svg'

class MainScreen extends Component {
  render() {
    return (
      <section className="partners-info">
        <div id="meet-the-partners" className="section-nav"></div>
        <div className="wrapper wrapper_small">
          <div className="partners-info__text">
            <div className="subtitle">We keep good company</div>
            <h2>Meet the partners.</h2>
            <div className="partners-info-list__wrap">
              <ul className="partners-info-list">
                <li className="partners-info-list__item">
                  <div className="partners-info-list__img">
                    <img src={PartnersImg1} alt="" />
                  </div>
                  <p>Advisory business for the global energy sector. </p>
                </li>
                <li className="partners-info-list__item">
                  <div className="partners-info-list__img">
                    <img src={PartnersImg2} alt="" />
                  </div>
                  <p>VC fund and partner specialised in Blockchain projects.</p>
                </li>
                <li className="partners-info-list__item">
                  <div className="partners-info-list__img">
                    <img src={PartnersImg3} alt="" />
                  </div>
                  <p>World-class digital development partner.</p>
                </li>
                <li className="partners-info-list__item">
                  <div className="partners-info-list__img">
                    <img src={PartnersImg4} alt="" />
                  </div>
                  <p>Australian advisory and excution partner to Blockchain companies.</p>
                </li>
                <li className="partners-info-list__item">
                  <div className="partners-info-list__img">
                    <img src={PartnersImg5} alt="" />
                  </div>
                  <p>Blockchain and dApps development agency.</p>
                </li>
                <li className="partners-info-list__item">
                  <div className="partners-info-list__img">
                    <img src={PartnersImg6} alt="" />
                  </div>
                  <p>Brand identity, creative UX and product design studio. </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
