import React, { Component } from 'react';
import Lottie from 'react-lottie';

import animationData1 from '../../../assets/svg-animations/1/data.json';
import animationData2 from '../../../assets/svg-animations/2/data.json';
import animationData3 from '../../../assets/svg-animations/3/data.json';

class MainScreen extends Component {
  render() {
    const defaultOptions1 = {
        loop: true,
        autoplay: true,
        animationData: animationData1,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    const defaultOptions2 = {
        loop: true,
        autoplay: true,
        animationData: animationData2,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    const defaultOptions3 = {
        loop: true,
        autoplay: true,
        animationData: animationData3,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    return (
      <section className="platform-info">
        <div id="what-is-tsd" className="section-nav"></div>
        <div className="wrapper wrapper_small">
          <div className="platform-info__item platform-info__item_1">
            <div className="platform-info__text">
              <div className="subtitle">Identify, originate and initiate projects</div>
              <h2>First ever blockchain platform for <span className="text-bg text-bg__small wow">sustainable&nbsp;</span> infrastructure assets.</h2>
              <p>Made for infrastructure entrepreneurs, land owners and developers to list and promote projects at the earliest stages of inception.</p>
              <p>The platform provides an end to end solution for all participants that want to originate, finance and develop complex infrastructure and sustainable energy projects. Decentralized ledger technology provides a solution to interact with multiple participants with certainty of contractual obligations and the ability to track every interaction.</p>
            </div>
            <div className="platform-info__img">
              <div className="platform-info__img-cont cont1">
                <div className="platform-info__animation-block" id="js-animation-block_2">
                  <Lottie options={defaultOptions2}
                  isStopped={false}
                  isPaused={false}/>
                </div>
              </div>
            </div>
          </div>
          <div className="platform-info__item platform-info__item_right platform-info__item_2">
            <div className="platform-info__text">
              <div className="subtitle">Use the power of decentralized ledger technology</div>
              <h2>A <span className="text-bg text-bg__small wow">community platform&nbsp;</span> to&nbsp;create sustainable infrastructure.</h2>
              <p>Transcendence is a blockchain-based platform for the development and management of large scale sustainable and socially valuable infrastructure projects.</p>
              <p>The Transcendence platform is designed to engage participants along the entire infrastructure development with a focus on providing a fairer playing field for professionals, the community and smaller participants that would normally have difficulty accessing these projects.</p>
            </div>
            <div className="platform-info__img">
              <div className="platform-info__img-cont cont2">
                <div className="platform-info__animation-block" id="js-animation-block_3">
                  <Lottie options={defaultOptions3}
                  isStopped={false}
                  isPaused={false}/>
                </div>
              </div>
            </div>
          </div>
          <div className="platform-info__item platform-info__item_3">
            <div className="platform-info__text">
              <div className="subtitle">Track the flow between assets and stakeholders</div>
              <h2><span className="text-bg text-bg__small wow">Seamless tracking&nbsp;</span> and transfer of royalties, surety bonds and payments.</h2>
              <p>Use the power of tokenization to implement secure Surety Bonds for projects, fractionalize assets and track royalty flows to all recipients and backers.</p>
              <p>With the advent of blockchain technology and smart contracts we now have the opportunity to automate key elements in the payments cycle associated with large scale development projects. Smart contracts allow for transparency around escrow, payment triggers and flow of funds.</p>
            </div>
            <div className="platform-info__img">
              <div className="platform-info__img-cont cont3">
                <div className="platform-info__animation-block" id="js-animation-block_1">
                  <Lottie options={defaultOptions1}
                  isStopped={false}
                  isPaused={false}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
