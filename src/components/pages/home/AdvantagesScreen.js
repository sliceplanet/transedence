import React, { Component } from 'react';



class MainScreen extends Component {
  render() {
    return (
      <div className="advantages">
        <div id="section-4" className="section-nav"></div>
        <div className="wrapper">
          <div className="box-advantages-list">
            <ul className="advantages-list">
              <li className="advantages-list__item">
                <div className="advantages-list__cont">
                  <div className="advantages-list__icon">
                    <i className="icon-icon-users"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span></i>
                  </div>
                  <div className="advantages-list__text">
                    Transparency across multiple stakeholders
                  </div>
                </div>
              </li>
              <li className="advantages-list__item">
                <div className="advantages-list__cont">
                  <div className="advantages-list__icon">
                    <i className="icon-icon-percent"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span></i>
                  </div>
                  <div className="advantages-list__text">
                    Increasing opportunity for solar energy projects
                  </div>
                </div>
              </li>
              <li className="advantages-list__item">
                <div className="advantages-list__cont">
                  <div className="advantages-list__icon">
                    <i className="icon-icon-bank"><span className="path1"></span><span className="path2"></span><span className="path3"></span><span className="path4"></span><span className="path5"></span><span className="path6"></span><span className="path7"></span></i>
                  </div>
                  <div className="advantages-list__text">
                    Overcoming domination by investment banks
                  </div>
                </div>
              </li>
              <li className="advantages-list__item">
                <div className="advantages-list__cont">
                  <div className="advantages-list__icon">
                    <i className="icon-icon-pie-chart"><span className="path1"></span><span className="path2"></span><span className="path3"></span></i>
                  </div>
                  <div className="advantages-list__text">
                    Equitable, shared distribution of project benefits
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default MainScreen;
