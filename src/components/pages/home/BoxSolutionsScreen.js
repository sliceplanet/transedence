import React, { Component } from 'react';

import VideoPoster from '../../../assets/img/img_video.jpg'

class MainScreen extends Component {
  render() {
    return (
      <section className="box-solution">
        <div id="introduction-video" className="section-nav"></div>
        <div className="wrapper wrapper_small">
          <div className="box-solution__text">
            <div className="subtitle">A clear alternative to investment banks</div>
            <h2>A disruptive blockchain solution for infrastructure in <span className="text-bg text-bg__small wow">renewable energy.&nbsp;</span></h2>
            <p>Transcendence is responding to the strong demand and support for eco-friendly, renewable energy projects, offering a financing model that equally benefits everyone.</p>
            <p>We are a clear alternative to investment banks with cost effective token capital structure and an open, transparent model.</p>
            <div className="box-button"><a href="#resources" className="button">READ WHITEPAPER</a></div>
          </div>
          <div className="video">
            <a href="https://www.youtube.com/embed/dM_fW_8ARg4?autoplay=1"  allow="accelerometer; autoplay; "  className="video__link fancy-iframe-js" data-fancybox-type="iframe">
              <img src={VideoPoster} alt="" />
              <span className="play-video">
                <span className="play-video__button">
                  PLAY VIDEO
                </span>
              </span>
            </a>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
