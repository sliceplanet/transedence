import React, { Component } from 'react';

class MainScreen extends Component {
  render() {
    return (
      <div className="key-info text-center">
        <div className="wrapper">
          <div className="key-info-list__wrap">
              <div className="key-info-list-block">
                            <ul className="key-info-list">
                                <li className="key-info-list__item col1">
                                    Transcendence <br />TGE Key <br />Numbers
                                </li>
                                <li className="key-info-list__item col2">
                                    <div className="key-info-list__title">Hard cap</div>
                                    <div className="key-info-list__value">US $<span>80.2</span>m</div>
                                    <div className="key-info-list__title">Soft cap</div>
                                    <div className="key-info-list__value">US $<span>20</span>m</div>
                                </li>
                                <li className="key-info-list__item col3">
                                    <div className="key-info-list__title">Total supply</div>
                                    <div className="key-info-list__value"><span>250</span>m TSD</div>
                                    <div className="key-info-list__title">Available for purchase</div>
                                    <div className="key-info-list__value"><span>200</span>m TSD</div>
                                </li>
                                <li className="key-info-list__item col4">
                                    <div className="key-info-list__title">Private sale</div>
                                    <div className="key-info-list__value">15 Nov &gt; 15 Feb <br />62.5m TSD <br />US $0.35 / token</div>
                                </li>
                                <li className="key-info-list__item col5">
                                    <div className="key-info-list__title">Pre sale</div>
                                    <div className="key-info-list__value">15 Mar > 15 Apr <br />62.5m TSD <br />US $0.35 / token</div>
                                </li>
                                <li className="key-info-list__item col5">
                                    <div className="key-info-list__title">Main sale</div>
                                    <div className="key-info-list__value">22 Apr &gt; 22 May <br />37.5m TSD<br />US $0.50 / token</div>
                                </li>
                            </ul>
                        </div>
          </div>
          <div className="journey-info__icon">
            <i className="icon-arr_slide_left"></i>
            <i className="icon-arr_slide_left right"></i>
          </div>
          <div className="journey-info__icon2">
            <i className="icon-hand"></i>
          </div>
        </div>
      </div>
    );
  }
}

export default MainScreen;
