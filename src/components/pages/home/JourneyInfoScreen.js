import React, { Component } from 'react';

class MainScreen extends Component {
  componentDidMount () {
    let self = this;

    let check = setInterval(() => {
      if (window.$('#forcecentered').sly) {
        self.runSlick()
        clearInterval(check)
      }
    });
  }

  runSlick () {
    var $frame = window.$('#forcecentered');
  	var $wrap  = $frame.parent();

  	if (($frame).length) {
  		$frame.sly({
  			horizontal: 1,
  			itemNav: 'forceCentered',
  			smart: 1,
  			activateMiddle: 1,
  			activateOn: 'click',
  			mouseDragging: 1,
  			touchDragging: 1,
  			releaseSwing: 1,
  			startAt: 5,
  			scrollBar: $wrap.find('.scrollbar'),
  			scrollBy: 1,
  			speed: 500,
  			elasticBounds: 1,
  			easing: 'easeOutExpo',
  			dragHandle: 1,
  			dynamicHandle: 1,
  			clickBar: 1,

  			// Buttons
  			prev: $wrap.find('.prev'),
  			next: $wrap.find('.next')
  		}).init();
  		$frame.sly('reload');
  	};
  }

  render() {
    return (
      <section className="journey-info">
        <div id="our-roadmap" className="section-nav"></div>
        <div className="wrapper">
          <div className="journey-info__box">
            <div className="subtitle">Our journey</div>
            <h2>Where we are, and <span className="text-bg text-bg__small wow">where we are going.</span></h2>
            <p>Our journey started many years ago, but with each milestone we are succesfully building to something more.</p>
          </div>
          <div className="wrap">
            <div className="frame journey-info__wrapper" id="forcecentered">
              <ul className="clearfix journey-info__slider">
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    2016
                    <div className="journey-info__text">
                      Retail Solar business was established and positioned as one of the first residential solar power retail companies in Australia.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    2011
                    <div className="journey-info__text">
                      Wholesale business arm established, one of the largest wholesalers of solar power systems in Australia within 1st year of operation.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    2016
                    <div className="journey-info__text">
                      The 13MW Mugga Lane Solar Park is commissioned and connected to the National Electricity Market.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    2017
                    <div className="journey-info__text">
                      Australia's largest solar deal is signed with the biggest energy retailer AGL. Australia's first corporate deal signed with UNSW and Origin Energy.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    2018
                    <div className="journey-info__text">
                      Transcendence Network is established and TGE is planned. Infrastructure development roadmap blueprint is created.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    Q4 2018
                    <div className="journey-info__text">
                      MVP for Transcendence Platform released. Start alpha development of the platform architecture and user on-boarding.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    Q1 2019
                    <div className="journey-info__text">
                      Private capital raised. Token Sale Campaign starts with Public Pre-sale on March 15th.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    Q2 2019
                    <div className="journey-info__text">
                      Alpha Transcendence Market Listing release. Start Alpha Transcendence Market Place (surety bonding and project financing)
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    Q3 2019
                    <div className="journey-info__text">
                      Beta release of Transcendence Market Place investment platform. First projects listed and funded.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    Q4 2019
                    <div className="journey-info__text">
                      Start construction of funded projects. Alpha release of the Transcendence exchange module.
                    </div>
                  </div>

                </li>
                <li className="journey-info__item">
                  <div className="journey-info__num">
                    Q1 2020
                    <div className="journey-info__text">
                      Transcendence marketing place investment and exchange modules fully functional.
                    </div>
                  </div>

                </li>
              </ul>
            </div>
            <div className="scrollbar">
              <div className="handle">
                <div className="mousearea">
                  <i className="icon-arr_slide_left"></i>
                  <i className="icon-arr_slide_left right"></i>
                </div>
              </div>
            </div>
          </div>
          <div className="journey-info__icon">
            <i className="icon-arr_slide_left"></i>
            <i className="icon-arr_slide_left right"></i>
          </div>
          <div className="journey-info__icon2">
            <i className="icon-hand"></i>
          </div>
          <div className="journey-info__btn">
            <a href="platform.html#our-roadmap" className="button">VIEW FULL TIMELINE</a>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
