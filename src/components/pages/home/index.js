import React, { Component } from 'react';

import MainScreen from './MainScreen.js'
import BoxSolutionsScreen from './BoxSolutionsScreen.js'
import PlarformInfoScreen from './PlatformScreen.js'
import AdvantagesScreen from './AdvantagesScreen.js'
import HowWorkScreen from './HowWorkScreen.js'
import TechnologyInfoScreen from './TechnologyInfoScreen.js'
import TranscendenceInfoScreen from './TranscendenceInfoScreen.js'
import JourneyInfoScreen from './JourneyInfoScreen.js'
import CompareInfoScreen from './CompareInfoScreen.js'
import AllocationInfoScreen from './AllocationInfoScreen.js'
import KeyInfoScreen from './KeyInfoScreen.js'
import TeamInfoScreen from './TeamInfoScreen.js'
import PartnersInfoScreen from './PartnersInfoScreen.js'
import Documents from '../../screens/DocumentsInfoScreen.js'
import WhiteList from '../../screens/WhiteListInfoScreen.js'

import SideNav from '../../ui/SideNav.js'
console.log(SideNav)

class HomePage extends Component {
  componentDidMount () {
      document.title = 'Transcendence Network | ICO/TGE Official Website | TSD'
      document.documentElement.scrollTop = 0;
  }

  render() {
    return (
      <main className="content">
        <SideNav />

        <MainScreen />
        <BoxSolutionsScreen />
  			<PlarformInfoScreen />
  			<AdvantagesScreen />
        <HowWorkScreen />
        <TechnologyInfoScreen />
        <TranscendenceInfoScreen />
        <JourneyInfoScreen />
        <CompareInfoScreen />
        <AllocationInfoScreen />
        <KeyInfoScreen />
        <TeamInfoScreen />
        <PartnersInfoScreen />
        <Documents />
        <WhiteList />
  		</main>
    );
  }
}

export default HomePage;
