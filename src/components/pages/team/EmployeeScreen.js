import React, { Component } from 'react'

import Team1 from '../../../assets/img/team/team_1.png'
import Team2 from '../../../assets/img/team/team_2.png'
import Team3 from '../../../assets/img/team/team_3.png'
import Team4 from '../../../assets/img/team/team_4.png'
import Team5 from '../../../assets/img/team/team_5.png'
import Team6 from '../../../assets/img/team/team_6.png'
import Team7 from '../../../assets/img/team/team_7.png'
import Team8 from '../../../assets/img/team/team_8.png'
import Team9 from '../../../assets/img/team/team_9.png'
import Team10 from '../../../assets/img/team/team_10.png'
import Team11 from '../../../assets/img/team/team_11.png'
import Team12 from '../../../assets/img/team/team_12.png'
import Team13 from '../../../assets/img/team/team_13.png'
import Team14 from '../../../assets/img/team/team_14.png'
import Team15 from '../../../assets/img/team/team_15.png'
import Team16 from '../../../assets/img/team/team_16.png'
// import Team17 from '../../../assets/img/team/team_17.png'
// import Team18 from '../../../assets/img/team/team_18.png'

export default class Screen extends Component {
  componentDidMount () {
    let $ = window.$;
    let self = this;

    let check = setInterval(() => {
      if ($('.team-employee__link')) {
        $('.team-employee__link').click(function(){
          $(this).parents('.team-employee__item').siblings().removeClass('active');
          $(this).parents('.team-employee__item').addClass('active');
          return false;
        });

        $(document).click(function(e){
      		if ($(e.target).parents().filter('.team-employee__description:visible').length != 1) {
      			$('.team-employee__item').removeClass('active');
      		}
      	});

        clearInterval(check)
      }
    })

    let check2 = setInterval(() => {
      if ($('.documents-info .documents-info__more')) {
        $('.documents-info .documents-info__more').click(function(){
          $('.documents-info-list').addClass('showed-all');
          $(this).hide();
          return false;
        });
        clearInterval(check2)
      }
    })
  }

  render () {
    return (
      <section className="team-employee">
        <div className="team-employee__circle"></div>
        <div className="wrapper">
          <div className="subtitle">
            Our team
          </div>
          <h2>
            Meet the
            <span className="text-bg text-bg__small wow">
              Transcendence team.&nbsp;
            </span>
          </h2>
          <p>
            A company is only as good as its people. The hard part is actually building the team that will embody the vision and propel you foward. We{"'"}ve started the process, and improving on it everyday.
          </p>
          <ul className="team-employee__list">
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/morris-zhou-3b810472/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team1} alt="" />
                </div>
                <div className="team-employee__name">
                  Morris Zhou
                </div>
                <div className="team-employee__specialty">
                  Chairman
                </div>
                <div className="team-employee__description">
                  Morris has over 10 years experience in establishing new business ventures and assembling quality team in high growth sectors and industries, ranging from manufacturing, retailing and sustainable social infrastructure development
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/qiao-nan-han-62617b51/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team2} alt="" />
                </div>
                <div className="team-employee__name">
                  Qiao Nan Han
                </div>
                <div className="team-employee__specialty">
                  Chief Executive Officer
                </div>
                <div className="team-employee__description">
                  Qiao has over 10 years experience across planning, development, engineering, contracting, construction, financing and sales of renewable energy and civil infrastructure assets.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/milton-zhou-44934074/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team3} alt="" />
                </div>
                <div className="team-employee__name">
                  Milton Zhou
                </div>
                <div className="team-employee__specialty">
                  Chief Marketing, Payment &amp; Compliance Officer
                </div>
                <div className="team-employee__description">
                  Milton has over 10 years experience being involved in the global Solar PV value chain and Fintech Payment sectors by bridging China and Australia, transforming numerous startups, by bringing them from zero to success.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/minimolist/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team4} alt="" />
                </div>
                <div className="team-employee__name">
                  Kevin Chen
                </div>
                <div className="team-employee__specialty">
                  Chief Investment Officer
                </div>
                <div className="team-employee__description">
                  Kevin comes from an investment banking background, having been involved in or led infrastructure transactions exceeding $40 billion in enterprise value, across the Infrastructure, Utilities and Renewables and Resources sectors.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/steve-cua-65074721/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team5} alt="" />
                </div>
                <div className="team-employee__name">
                  Steve Cua
                </div>
                <div className="team-employee__specialty">
                  Chief Technology Officer
                </div>
                <div className="team-employee__description">
                  Steve has extensive experience designing and delivering complex systems across the globe for millions of users. He has helped a wide range of companies develop digital solutions, with a strong focus on design, delivery, stakeholder engagement and management.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/alexjjl/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team6} alt="" />
                </div>
                <div className="team-employee__name">
                  Alex Luttringer
                </div>
                <div className="team-employee__specialty">
                  Head of Marketing
                </div>
                <div className="team-employee__description">
                  Alex has 12 years’ experience conceptualising and executing end-to-end digital marketing strategies for top-level startups around the world and has been involved in the blockchain and crypto space for over 3 years.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>





            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/antonia-peart-7a791a172/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team7} alt="" />
                </div>
                <div className="team-employee__name">
                  Antonia Peart
                </div>
                <div className="team-employee__specialty">
                  Legal and Commercial Director
                </div>
                <div className="team-employee__description">
                  Antonia comes from a legal background, having worked at top tier law firms. She has extensive experience working on major infrastructure projects globally. She also has experience advising on blockchain and cryptocurrency since 2015.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/siekeu/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team8} alt="" />
                </div>
                <div className="team-employee__name">
                  Chris Quevedo
                </div>
                <div className="team-employee__specialty">
                  Head of Design
                </div>
                <div className="team-employee__description">
                  Chris has 10 years’ experience in driving user strategy and delivery within startups, telecommunications, enterprise and the financial space, and is a crypto enthusiast amongst other emerging technologies.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/thomas-upton-99445616b/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team9} alt="" />
                </div>
                <div className="team-employee__name">
                  Thomas Upton
                </div>
                <div className="team-employee__specialty">
                  UI/UX Designer
                </div>
                <div className="team-employee__description">
                  Thomas has 6 years UX, UI and Visual Design experience that enables him to connect brands and companies with their customers through good design.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/isabelle-o-1b9820173/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team10} alt="" />
                </div>
                <div className="team-employee__name">
                  Isabelle O’Hara
                </div>
                <div className="team-employee__specialty">
                  Marketing &amp; Events Executive
                </div>
                <div className="team-employee__description">
                  Isabelle has over 7 years communications experience, with a background in the tech sector. She has worked internationally in New
                  York, Dublin and Sydney for companies such as Microsoft and Foursquare.


                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/yolanda-zhao-a71867a4/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team11} alt="" />
                </div>
                <div className="team-employee__name">
                  Yolanda Zhao
                </div>
                <div className="team-employee__specialty">
                  Executive Relationship
                  Manager
                </div>
                <div className="team-employee__description">
                  Yolanda’s was the youngest ever cabin purser while receiving numerous prestigious awards from CAAC during her tenure with Shanghai Airlines. Currently has worked on transactions with over $200+ million in enterprise value.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/michael-tran-45871a5b/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team12} alt="" />
                </div>
                <div className="team-employee__name">
                  Michael Tran
                </div>
                <div className="team-employee__specialty">
                  Infrastructure Manager
                  Planning & Engineering
                </div>
                <div className="team-employee__description">
                  Michael has more than 8 years of experience in systems design, project and resource management, testing and commissioning, operations, FMECA,
                  O&M and RAMS. Michael brings with him years of experience leading teams through changing business environment.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/dan-drory-33496b95/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team13} alt="" />
                </div>
                <div className="team-employee__name">
                  Dan Drory
                </div>
                <div className="team-employee__specialty">
                  Blockchain Developer
                </div>
                <div className="team-employee__description">


                </div>
                <a href="#" className="team-employee__link hide">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/spencerdezartsmith/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team14} alt="" />
                </div>
                <div className="team-employee__name">
                  Spencer Depart-Smith
                </div>
                <div className="team-employee__specialty">
                  Blockchain Developer
                </div>
                <div className="team-employee__description">


                </div>
                <a href="#" className="team-employee__link hide">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/srikanth-chevalam/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team15} alt="" />
                </div>
                <div className="team-employee__name">
                  Srikanth Chevalam
                </div>
                <div className="team-employee__specialty">
                  Blockchain Developer
                </div>
                <div className="team-employee__description">

                </div>
                <a href="#" className="team-employee__link hide">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/mizko/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team16} alt="" />
                </div>
                <div className="team-employee__name">
                  Michael Wong
                </div>
                <div className="team-employee__specialty">
                  Lead Designer
                </div>
                <div className="team-employee__description">


                </div>
                <a href="#" className="team-employee__link hide">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            {/*<li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/georgehatzis/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team17} alt="" />
                </div>
                <div className="team-employee__name">
                  George Hatzis
                </div>
                <div className="team-employee__specialty">
                  User Experience Designer
                </div>
                <div className="team-employee__description">


                </div>
                <a href="#" className="team-employee__link hide">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <!--<li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/vickimo/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={Team18} alt="" />
                </div>
                <div className="team-employee__name">
                  Vicki Mo
                </div>
                <div className="team-employee__specialty">
                  Design Project Manager
                </div>
                <div className="team-employee__description">


                </div>
                <a href="#" className="team-employee__link hide">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>*/}
          </ul>

          <a href="#" className="documents-info__more">See more
            <div className="documents-info__icon">
              <i className="icon-arrow_bottom"></i>
            </div>
          </a>
        </div>
      </section>
    )
  }
}
