import React, { Component } from 'react'

import TeamIcon19 from '../../../assets/img/team/team_19.png'
import TeamIcon20 from '../../../assets/img/team/team_20.png'
import TeamIcon21 from '../../../assets/img/team/team_21.png'
import TeamIcon22 from '../../../assets/img/team/team_22.png'

export default class Screen extends Component {
  componentDidMount () {
    let $ = window.$;
    let self = this;

    let check = setInterval(() => {
      if ($('.team-employee__link')) {
        $('.team-employee__link').click(function(){
          $(this).parents('.team-employee__item').siblings().removeClass('active');
          $(this).parents('.team-employee__item').addClass('active');
          return false;
        });

        $(document).click(function(e){
      		if ($(e.target).parents().filter('.team-employee__description:visible').length != 1) {
      			$('.team-employee__item').removeClass('active');
      		}
      	});

        clearInterval(check)
      }
    })

    let check2 = setInterval(() => {
      if ($('.documents-info .documents-info__more')) {
        $('.documents-info .documents-info__more').click(function(){
          $('.documents-info-list').addClass('showed-all');
          $(this).hide();
          return false;
        });
        clearInterval(check2)
      }
    })
  }

  render () {
    return (
      <section className="team-advisory orange center">
        <div className="team-advisory__circle">

        </div>
        <div className="wrapper">
          <h2>
            Meet the&nbsp;
            <span className="text-bg text-bg__small wow">
              Advisory team.
            </span>
          </h2>
          <p>
            With the most relevant industry advisors, less time is wasted on figuring out what could work, more time working on delivering what will work.
          </p>
          <ul className="team-employee__list">
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/ivanmantelli/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={TeamIcon19} alt="" />
                </div>
                <div className="team-employee__name">
                  Ivan Mantelli:
                </div>
                <div className="team-employee__specialty">
                  ICO Advisor
                </div>
                <div className="team-employee__description">
                  Ivan is an experienced startup and corporate advisor with over 18 years’ experience, including investment banking at ABN Amro Australia, and corporate strategy and M&amp;A at Fairfax Media. Ivan holds a BSC in Pharmacology UNSW and MBus UTS.

                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/vaibhavnamburi/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={TeamIcon20} alt="" />
                </div>
                <div className="team-employee__name">
                  Vaibhav Nambur
                </div>
                <div className="team-employee__specialty">
                  Blockchain Developer
                </div>
                <div className="team-employee__description">
                  Vaibhav’s a veteran software developer with an extensive background in building decentralised apps with scalable and safe smart contract protocols. He’s on the advisory board for several well reputed multi-million ICOs.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/p-kang/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={TeamIcon21} alt="" />
                </div>
                <div className="team-employee__name">
                  Paul Kang
                </div>
                <div className="team-employee__specialty">
                  Security Advisor
                </div>
                <div className="team-employee__description">
                  Paul is the Co-Founder of Entersoft, a global award- winning Cyber Security organisation. Entersoft team has helped launch and secure ICOs worth over $1 billion all over the world. Winner of multiple awards including Hong Kong Fintech of the Year.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            <li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/simon-corbell-20311468/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src={TeamIcon22} alt="" />
                </div>
                <div className="team-employee__name">
                  Simon Corbell
                </div>
                <div className="team-employee__specialty">
                  Independant Advisor Board and Ivestment Committee
                </div>
                <div className="team-employee__description">
                  With close to two decades of senior public policy experience in renewable energy and sustainability policy and projects, Simon is one of Australia’s leading renewable energy advocates.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>
            {/*<li className="team-employee__item">
              <div className="team-employee__box">
                <a href="https://www.linkedin.com/in/simon-currie-26780929/" target="_blank" className="team-employee__social">
                  <i className="icon-linkedin-white"></i>
                </a>
                <div className="team-employee__photo">
                  <img src="img/team/team_23.png" alt="" />
                </div>
                <div className="team-employee__name">
                  Simon Currie
                </div>
                <div className="team-employee__specialty">
                  Independant Advisor Board
                  and Ivestment Committee
                </div>
                <div className="team-employee__description">
                  Simon is a principal of Energy Estate - a energy advisory and accelerator business. He was previously a partner with a leading international law firm for 18 years and led their energy industry group for 15 years.
                </div>
                <a href="#" className="team-employee__link">
                  <i className="icon-preview"></i>
                  <span> View</span>
                </a>
              </div>

            </li>*/}
          </ul>
        </div>
      </section>
    )
  }
}
