import React, { Component } from 'react';

import SmallPoster1 from '../../../assets/img/img_small_video_1.jpg'
import SmallPoster2 from '../../../assets/img/img_small_video_2.jpg'

class MainScreen extends Component {
  render() {
    return (
      <section className="technology-info orange">
        <div id="our-track-record" className="section-nav"></div>
        <div className="wrapper wrapper_small">
          <div className="technology-info__text">
            <div className="subtitle">Our track record</div>
            <h2>Transcendence, the new <br />blockchain-arm of the <br /><span className="text-bg text-bg__small wow">Maoneng Group.&nbsp;</span></h2>
            <p>We understand the difficulties and pain points of the industry. Our team has joint experience in delivering over $50 billion worth of projects globally. </p>
            <p>Our holding company, Maoneng Group, is currently building Sunraysia Solar Farm - 255MWp, the largest to be built in Australia.</p>
          </div>
          <div className="technology-info-list__wrap">
            <ul className="technology-info-list">
              <li className="technology-info-list__item">
                <div className="technology-info-list__item-wrap">
                  <a href="https://www.youtube.com/embed/d-xK2DySP70?autoplay=1" className="technology-info-list__img fancy-iframe-js" data-fancybox-type="iframe"allow="accelerometer; autoplay;">
                    <img src={SmallPoster1} alt="" />
                    <span className="play-video">
                      <span className="play-video__button">
                        PLAY VIDEO
                      </span>
                    </span>
                  </a>





                  <a href="#" className="technology-info-list__title">Mugga Lane</a>
                  <p>First Major project, developed, owned and operated. Australia Capital Territory’s premier solar power plant. Capacity 24,500 MWh. Powering 3000+ homes.</p>
                  <a href="#" className="technology-info-list__btn">Play video</a>
                </div>
              </li>
              <li className="technology-info-list__item">
                <div className="technology-info-list__item-wrap">
                  <a href="https://www.youtube.com/embed/UW2bLPEkrT0?autoplay=1" className="technology-info-list__img fancy-iframe-js" data-fancybox-type="iframe"allow="accelerometer; autoplay;">
                    <img src={SmallPoster2} alt="" />
                    <span className="play-video">
                      <span className="play-video__button">
                        PLAY VIDEO
                      </span>
                    </span>
                  </a>
                  <a href="#" className="technology-info-list__title">Sunraysia</a>
                  <p>Newest development in construction. One of the largest <br /> in the world and biggest in Australia. Capacity 255MWDC Over 10km². Will power 50,000+ homes.</p>
                  <a href="#" className="technology-info-list__btn">Play video</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </section>
    );
  }
}

export default MainScreen;
