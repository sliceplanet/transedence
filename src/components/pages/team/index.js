import React, { Component } from 'react';

import Main from './MainScreen.js'
import Employee from './EmployeeScreen.js'
import Advisory from './AdvisoryScreen.js'
import TechnologyInfo from './TechnologyInfoScreen.js'
import Documents from '../../screens/DocumentsInfoScreen.js'
import WhiteList from '../../screens/WhiteListInfoScreen.js'

class TeamPage extends Component {
  componentDidMount () {
      document.title = 'Out Team | Transcendence Network (TSD)'
      document.documentElement.scrollTop = 0;
  }

  render() {
    return (
      <main className="content">
  			<Main />
  			<Employee />
  			<Advisory />
  			<TechnologyInfo />
        <Documents />
        <WhiteList />
  		</main>
    );
  }
}

export default TeamPage;
