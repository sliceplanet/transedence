import React, { Component } from 'react'

import ImgTeamHeader from '../../../assets/img/TeamHeader.jpg'
import Mp4TeamHeader from '../../../assets/img/video/TeamHeader.mp4'
import WebmTeamHeader from '../../../assets/img/video/TeamHeader.webm'

export default class Screen extends Component {
  render () {
    return (
      <section className="team-main">
        <div className="wrapper">
          <div className="team-main__box">
            <div className="team-main__left">
              <h1>
                Experienced and <br />
                <span className="text-bg text-bg__big wow">
                  leading developer
                </span>&nbsp;
                in renewables.
              </h1>
              <p>
                Transcendence is being built by the people behind Maoneng, whose team has joint experience of delivering over 50 billion dollars worth of infrastructure projects globally.

              </p>
              <p>
                Maoneng is an experienced and leading developer in renewables, currently constructing the largest solar power plant in the Southern Hemisphere.
              </p>
              <a target="_blank" href="https://tsd.network/whitelist" className="button">JOIN THE WHITELIST</a>
            </div>
            <div className="team-main__right">
              <a href="https://www.youtube.com/embed/ufG0-hLUjsQ?autoplay=1" className="team-main__photo fancy-iframe-js" data-fancybox-type="iframe">
                <video loop muted autoPlay poster={ImgTeamHeader} className="main-header__video">
                  <source src={Mp4TeamHeader} type='video/mp4' className="sour1" />
                  <source src={WebmTeamHeader} type='video/webm' className="sour2" />
                </video>

                <span className="play-video">
                    <span className="play-video__button">
                        PLAY VIDEO
                    </span>
                </span>
              </a>

            </div>
          </div>
        </div>
      </section>
    )
  }
}
