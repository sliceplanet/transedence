$(window).on('load', function () {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded');

    // setTimeout(function() {
    //     $('.input input').attr('placeholder', 'Enter email address')
    // }, 300);

    // if($('#countdown').length){
    //     var ts = new Date(2019, 1, 15),
    //         newYear = true;
		//
    //     $('#countdown').countdown({
    //         timestamp	: ts,
    //         callback	: function(days, hours, minutes, seconds){
		//
    //             var message = "";
		//
    //             message += "Дней: " + days +", ";
    //             message += "часов: " + hours + ", ";
    //             message += "минут: " + minutes + " и ";
    //             message += "секунд: " + seconds + " <br />";
		//
    //         }
    //     });
    // }
});

/* viewport width */
function viewport(){
	var e = window,
		a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */


$(function(){
	/* placeholder*/
	$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){
 			$(this).attr('placeholder', placeholder);
 		});
 	});
	/* placeholder*/

	$('.button-nav').click(function(){
		$(this).toggleClass('active'),
		$('.main-nav').toggleClass('open');
		return false;
	});

	$('.main-nav__link').click(function () {
		$('.button-nav').toggleClass('active');
		$('.main-nav').toggleClass('open');
	})

  // $('.js-cookie-close').click(function(){
	// 	$('.box-cookie').fadeOut();
  //   $('body').removeClass('cookie');
	// 	localStorage.setItem('cookie', '1');
	// 	return false;
	// });
  //   $('.js-hide-country').click(function(){
	// 	$('.document-country').fadeOut();
	// 	return false;
	// });
    $('.documents-info .documents-info__more').click(function(){
		$('.documents-info-list').addClass('showed-all');
        $(this).hide();
		return false;
	});
    $('.team-employee .documents-info__more').click(function(){
		$('.team-employee__list').addClass('showed-all');
        $(this).hide();
		return false;
	});

	/* components */

	/*

	if($('.styled').length) {
		$('.styled').styler();
	};
	if($('.fancybox').length) {
		$('.fancybox').fancybox({
			margin  : 10,
			padding  : 10
		});
	};

	if($('.scroll').length) {
		$(".scroll").mCustomScrollbar({
			axis:"x",
			theme:"dark-thin",
			autoExpandScrollbar:true,
			advanced:{autoExpandHorizontalScroll:true}
		});
	};

	*/

	/*Tabs*/
	$('.platform-journey-year li a').click(function() {

    	$(this).parent().siblings().removeClass('active');
    	var id = $(this).attr('href');

    	$(this).parent().addClass('active');

	    return false;
	});

	// if($('.transcendence-info__box-js').length) {
	// 	$('.transcendence-info__box-js').slick({
	// 		dots: true,
	// 		infinite: true,
	// 		speed: 500,
  //           fade:true,
	// 		slidesToShow: 1,
	// 		dots: false,
	// 		responsive: [
	// 			{
	// 			  breakpoint: 768,
	// 			  settings: {
	//
	// 				arrows: false,
	// 				dots: true
	// 			  }
	// 			}
	//
	// 		]
	// 	});
	// };
	//
	// if($('.transcendence-info__box-js .slick-arrow').length) {
	// 	$('.transcendence-info__box-js .slick-arrow').text('');
	// }

	// //SVG Animation
	// if($('#js-animation-block_1').length) {
	// 	var animationFirst = bodymovin.loadAnimation({
	// 		container: document.getElementById('js-animation-block_1'),
	// 		renderer: 'svg',
	// 		loop: true,
	// 		autoplay: true,
	// 		path: 'img/svg-animations/1/data.json'
	// 	});
	// };
	//
	// if($('#js-animation-block_2').length) {
	// 	var animationSecond = bodymovin.loadAnimation({
	// 		container: document.getElementById('js-animation-block_2'),
	// 		renderer: 'svg',
	// 		loop: true,
	// 		autoplay: true,
	// 		path: 'img/svg-animations/2/data.json'
	// 	});
	// };
	//
	// if($('#js-animation-block_3').length) {
	// 	var animationThird = bodymovin.loadAnimation({
	// 		container: document.getElementById('js-animation-block_3'),
	// 		renderer: 'svg',
	// 		loop: true,
	// 		autoplay: true,
	// 		path: 'img/svg-animations/3/data.json'
	// 	});
	// };

	//WOW
	// if ($('.wow').length) {
	// 	wow = new WOW({
	// 		boxClass:     'wow',      // default
	// 		animateClass: 'animated', // default
	// 		offset:       0,          // default
	// 		mobile:       true,       // default
	// 		live:         true        // default
	// 	})
	// 	wow.init()
	// };

	//OnePageNav
	if ($('#js-sidebar-nav-list').length) {
		// $('#js-sidebar-nav-list').onePageNav({
		// 	navItems: 'a',
		// 	currentClass: 'active',
		// 	changeHash: true,
		// 	scrollSpeed: 750,
		// 	scrollThreshold: 0.5,
		// 	easing: 'swing',
		// });
	};

	//OnePageNav hover
	// $('#js-sidebar-nav-list').hover(
	// 	function() {
	// 		$('.overlay').addClass('active');
	// 	},
	// 	function() {
	// 		$('.overlay').removeClass('active');
	// 	}
	// );

  // $('.team-employee__link').click(function(){
	// 	$(this).parents('.team-employee__item').siblings().removeClass('active');
	// 	$(this).parents('.team-employee__item').addClass('active');
	// 	return false;
	// });

	// $(document).click(function(e){
	// 	if ($(e.target).parents().filter('.team-employee__description:visible').length != 1) {
	// 		$('.team-employee__item').removeClass('active');
	// 	}
	// });


	//highcharts graph
	// var colors = Highcharts.getOptions().colors,
	// 	categories = [
	// 		"Private round",
	// 		"Pre-sale round",
	// 		"Public round",
	// 		"Founders, team and advisors",
	// 		"Bounty and rewards",
	// 		"Liquidity program",
	// 		"Project Implementation providers"
	// 	],
	// 	data = [
	// 		{
	// 			"y": 62.74,
	// 			"color": '#96BAE2',
	// 			"drilldown": {
	// 				"name": "Private round",
	// 				"categories": [
	// 					"Private round",
	// 				],
	// 				"data": [
	// 					40.0,
	// 				]
	// 			}
	// 		},
	// 		{
	// 			"y": 10.57,
	// 			"color": '#679CD2',
	// 			"drilldown": {
	// 				"name": "Pre-sale round",
	// 				"categories": [
	// 					"Pre-sale round",
	// 				],
	// 				"data": [
	// 					25,
	// 				]
	// 			}
	// 		},
	// 		{
	// 			"y": 7.23,
	// 			"color": '#4266B0',
	// 			"drilldown": {
	// 				"name": "Public round",
	// 				"categories": [
	// 					"Public round",
	// 				],
	// 				"data": [
	// 					15,
	// 				]
	// 			}
	// 		},
	// 		{
	// 			"y": 5.58,
	// 			"color": '#1C5FA6',
	// 			"drilldown": {
	// 				"name": "Founders, team and advisors",
	// 				"categories": [
	// 					"Founders, team and advisors",
	// 				],
	// 				"data": [
	// 					8,
	// 				]
	// 			}
	// 		},
	// 		{
	// 			"y": 4.02,
	// 			"color": '#153E69',
	// 			"drilldown": {
	// 				"name": "Bounty and rewards",
	// 				"categories": [
	// 					"Bounty and rewards",
	// 				],
	// 				"data": [
	// 					7,
	// 				]
	// 			}
	// 		},
	// 		{
	// 			"y": 1.92,
	// 			"color": '#0E2136',
	// 			"drilldown": {
	// 				"name": "Liquidity program",
	// 				"categories": [
	// 					"Liquidity program",
	// 				],
	// 				"data": [
	// 					3,
	// 				]
	// 			}
	// 		},
	// 		{
	// 			"y": 7.62,
	// 			"color": '#0E2136',
	// 			"drilldown": {
	// 				"name": 'Project Implementation providers',
	// 				"categories": [
	// 					'Project Implementation providers'
	// 				],
	// 				"data": [
	// 					2
	// 				]
	// 			}
	// 	}
	// 	],
	// 	browserData = [],
	// 	versionsData = [],
	// 	i,
	// 	j,
	// 	dataLen = data.length,
	// 	drillDataLen,
	// 	brightness;
	//
	//
	// // Build the data arrays
	// for (i = 0; i < dataLen; i += 1) {
	//
	// 	// add browser data
	// 	browserData.push({
	// 		name: categories[i],
	// 		y: data[i].y,
	// 		color: data[i].color
	// 	});
	//
	// 	// add version data
	// 	drillDataLen = data[i].drilldown.data.length;
	// 	for (j = 0; j < drillDataLen; j += 1) {
	// 		brightness = 0.2 - (j / drillDataLen) / 5;
	// 		versionsData.push({
	// 			name: data[i].drilldown.categories[j],
	// 			y: data[i].drilldown.data[j],
	// 			color: Highcharts.Color(data[i].color).brighten().get()
	// 		});
	// 	}
	// }
	//
	// // Create the chart
	// if ($('#graph1').length) {
	// 	Highcharts.chart('graph1', {
	// 		chart: {
	// 			type: 'pie'
	// 		},
	// 		plotOptions: {
	// 			pie: {
	// 				shadow: false,
	// 				center: ['50%', '50%']
	// 			}
	// 		},
	// 		tooltip: {
	// 			valueSuffix: '%'
	// 		},
	// 		series: [{
	// 			name: 'Distribution',
	// 			data: browserData,
	// 			size: '60%',
	// 			dataLabels: {
	// 				formatter: function () {
	// 					return this.y > 5 ? this.point.name : null;
	// 				},
	// 				color: '#ffffff',
	// 				distance: -30
	// 			}
	// 		}, {
	// 			name: 'Result',
	// 			data: versionsData,
	// 			size: '79%',
	// 			innerSize: '79%',
	//
	// 			id: 'versions'
	// 		}],
	// 		responsive: {
	// 			rules: [{
	// 				condition: {
	// 					maxWidth: 400
	// 				},
	// 				chartOptions: {
	// 					series: [{
	// 						id: 'versions',
	// 						dataLabels: {
	// 							enabled: false
	// 						}
	// 					}]
	// 				}
	// 			}]
	// 		}
	// 	});
	// };


	// var colors = Highcharts.getOptions().colors,
	// 	categories = [
	// 		"Project development",
	// 		"Platform development",
	// 		"Contingency",
	// 		"Offer, liquidity and listing costs",
	// 		"Business development",
	// 		"Business development"
	// 	],
	// 	data = [
	// 		{
	//
	// 			"color": 'rgb(255,216,141)',
	// 			"drilldown": {
	// 				"name": "Project development",
	// 				"categories": [
	// 					"Project development",
	// 				],
	// 				"data": [
	// 					68.24,
	// 				]
	// 			}
	// 		},
	// 		{
	//
	// 			"color": '#FECB66',
	// 			"drilldown": {
	// 				"name": "Contingency",
	// 				"categories": [
	// 					"Contingency",
	// 				],
	// 				"data": [
	// 					20.74,
	// 				]
	// 			}
	// 		},
	// 		{
	//
	// 			"color": '#F3B246',
	// 			"drilldown": {
	// 				"name": "Offer, liquidity and listing costs",
	// 				"categories": [
	// 					"Offer, liquidity and listing costs",
	// 				],
	// 				"data": [
	// 					3.51,
	// 				]
	// 			}
	// 		},
	// 		{
	//
	// 			"color": '#AE8336',
	// 			"drilldown": {
	// 				"name": "Platform development",
	// 				"categories": [
	// 					"Platform development",
	// 				],
	// 				"data": [
	// 					2.54,
	// 				]
	// 			}
	// 		},
	// 		{
	//
	// 			"color": '#705525',
	// 			"drilldown": {
	// 				"name": "Business development + Marketing",
	// 				"categories": [
	// 					"Business development + Marketing",
	// 				],
	// 				"data": [
	// 					2.48,
	// 				]
	// 			}
	// 		},
	// 		{
	//
	// 			"color": '#40321A',
	// 			"drilldown": {
	// 				"name": "Operational costs and overheads",
	// 				"categories": [
	// 					"Operational costs and overheads",
	// 				],
	// 				"data": [
	// 					2.48,
	// 				]
	// 			}
	// 		}
	// 	],
	// 	browserData = [],
	// 	versionsData = [],
	// 	i,
	// 	j,
	// 	dataLen = data.length,
	// 	drillDataLen,
	// 	brightness;
	//
	//
	// // Build the data arrays
	// for (i = 0; i < dataLen; i += 1) {
	//
	// 	// add browser data
	// 	browserData.push({
	// 		name: categories[i],
	// 		y: data[i].y,
	// 		color: data[i].color
	// 	});
	//
	// 	// add version data
	// 	drillDataLen = data[i].drilldown.data.length;
	// 	for (j = 0; j < drillDataLen; j += 1) {
	// 		brightness = 0.2 - (j / drillDataLen) / 5;
	// 		versionsData.push({
	// 			name: data[i].drilldown.categories[j],
	// 			y: data[i].drilldown.data[j],
	// 			color: Highcharts.Color(data[i].color).brighten().get()
	// 		});
	// 	}
	// }
	// // Create the chart
	// if ($('#graph2').length) {
	// 	Highcharts.chart('graph2', {
	// 		chart: {
	// 			type: 'pie'
	// 		},
	//
	// 		plotOptions: {
	// 			pie: {
	// 				shadow: false,
	// 				center: ['50%', '50%']
	// 			}
	// 		},
	// 		tooltip: {
	// 			valueSuffix: '%'
	// 		},
	// 		series: [{
	// 			name: 'Distribution',
	// 			data: browserData,
	// 			size: '60%',
	// 			dataLabels: {
	// 				formatter: function () {
	// 					return this.y > 5 ? this.point.name : null;
	// 				},
	// 				distance: -30
	// 			}
	// 		}, {
	// 			name: 'Result',
	// 			data: versionsData,
	// 			size: '78%',
	// 			innerSize: '78%',
	//
	// 			id: 'versions'
	// 		}],
	// 		responsive: {
	// 			rules: [{
	// 				condition: {
	// 					maxWidth: 400
	// 				},
	// 				chartOptions: {
	// 					series: [{
	// 						id: 'versions',
	// 						dataLabels: {
	// 							enabled: false
	// 						}
	// 					}]
	// 				}
	// 			}]
	// 		}
	// 	});
	// };

	/*END highcharts graph*/






	/* components */

});

/*fancyBox*/
  // if($('.fancy-iframe-js').length){
  //     $('.fancy-iframe-js').fancybox({
  //         padding: 0,
  //         margin: 10,
  //         wrapCSS: 'fancy-main'
  //     })
  // }


/*EndFancyBox*/
var viewport_height = viewport().height;
    $(window).scroll(function () {

        if ($(window).scrollTop() < viewport_height) {
            $('.sidebar-nav-list__wrap').addClass('hide-sunbav');
        } else{
            $('.sidebar-nav-list__wrap').removeClass('hide-sunbav');
        }
    });

    $(window).load(function () {

        if ($(window).scrollTop() < viewport_height) {
            $('.sidebar-nav-list__wrap').addClass('hide-sunbav');
        } else{
            $('.sidebar-nav-list__wrap').removeClass('hide-sunbav');
        }
    });

var handler = function(){

	var height_footer = $('footer').height();
	var height_header = $('header').height();
	//$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});


	var viewport_wid = viewport().width;
	var viewport_height = viewport().height;

	if (viewport_wid <= 991) {

	}


    // $(window).scroll(function () {
    //     if ($(window).scrollTop() > 1) {
    //         $('header').addClass("fixed");
    //     } else {
    //         $('header').removeClass("fixed")
    //     };
		//
    // });
		//
    // $(window).load(function () {
    //     if ($(window).scrollTop() > 1) {
    //         $('header').addClass("fixed");
    //     } else {
    //         $('header').removeClass("fixed")
    //     };
		//
    // });

	/*Sly Option*/

	/*Main Page*/
	// var $frame = $('#forcecentered');
	// var $wrap  = $frame.parent();
	//
	// if (($frame).length) {
	// 	$frame.sly({
	// 		horizontal: 1,
	// 		itemNav: 'forceCentered',
	// 		smart: 1,
	// 		activateMiddle: 1,
	// 		activateOn: 'click',
	// 		mouseDragging: 1,
	// 		touchDragging: 1,
	// 		releaseSwing: 1,
	// 		startAt: 5,
	// 		scrollBar: $wrap.find('.scrollbar'),
	// 		scrollBy: 1,
	// 		speed: 500,
	// 		elasticBounds: 1,
	// 		easing: 'easeOutExpo',
	// 		dragHandle: 1,
	// 		dynamicHandle: 1,
	// 		clickBar: 1,
	//
	// 		// Buttons
	// 		prev: $wrap.find('.prev'),
	// 		next: $wrap.find('.next')
	// 	}).init();
	// 	$frame.sly('reload');
	// };

	/*Platform Page*/
	// var $frame = $('.js-platform-journey');
	// var $wrap  = $frame.parent();
	//
	// if (($frame).length) {
	//
	// 	$frame.sly({
	// 		horizontal: false,
	// 	    itemNav: 'forceCentered',
	// 		smart: 1,
	// 		activateMiddle: true,
	// 		activateOn: 'click',
	// 		mouseDragging: 1,
	// 		touchDragging: 1,
	// 		releaseSwing: 1,
	// 		startAt: 9,
	// 		scrollBar: $wrap.closest('.platform-journey__wrap').find('.scrollbar'),
	// 		scrollBy: 1,
	// 		speed: 500,
	// 		elasticBounds: 1,
	// 		easing: 'easeOutExpo',
	// 		dragHandle: 1,
	// 		dynamicHandle: 1,
	// 		clickBar: 1,
  //           pagesBar: $('.nav-pl-js'),
	// 		activatePageOn: 'click',
	// 		// Buttons
	// 		prev: $wrap.find('.prev'),
	// 		next: $wrap.find('.next'),
	//
	//
	// 	}).init();
	// 	$frame.sly('reload');
  //       $frame.sly('on', 'load move', function(){
	//
  //      if($('.nav-pl-js li').eq(0).hasClass('active')){
  //        $('.platform-journey-year__item').removeClass('active')
  //        $('.platform-journey-year__item.item1').addClass('active')
  //       }
  //       if($('.nav-pl-js li').eq(5).hasClass('active')){
  //        $('.platform-journey-year__item').removeClass('active')
  //        $('.platform-journey-year__item.item2').addClass('active')
  //       }
  //        if($('.nav-pl-js li').eq(7).hasClass('active')){
  //        $('.platform-journey-year__item').removeClass('active')
  //        $('.platform-journey-year__item.item3').addClass('active')
  //       }
  //        if($('.nav-pl-js li').eq(11).hasClass('active')){
  //        $('.platform-journey-year__item').removeClass('active')
  //        $('.platform-journey-year__item.item4').addClass('active')
  //       }
  //        if($('.nav-pl-js li').eq(15).hasClass('active')){
  //        $('.platform-journey-year__item').removeClass('active')
  //        $('.platform-journey-year__item.item5').addClass('active')
  //       }
  //        if($('.nav-pl-js li').eq(19).hasClass('active')){
  //        $('.platform-journey-year__item').removeClass('active')
  //        $('.platform-journey-year__item.item6').addClass('active')
  //       }
  //        if($('.nav-pl-js li').eq(20).hasClass('active')){
  //        $('.platform-journey-year__item').removeClass('active')
  //        $('.platform-journey-year__item.item7').addClass('active')
  //       }
	//
	//
	//
  //            $('.click-year2016').click(function(){
	//
  //    $('.nav-pl-js li').eq(0).click()
  //    return false
	//
  //    })
	//
  //    $('.click-year2017').click(function(){
	//
  //    $('.nav-pl-js li').eq(5).click()
  //    return false
	//
  //    })
  //    $('.click-year2018').click(function(){
	//
  //    $('.nav-pl-js li').eq(7).click()
  //    return false
	//
  //    })
  //    $('.click-year2019').click(function(){
	//
  //    $('.nav-pl-js li').eq(10).click()
  //    return false
	//
  //    })
  //    $('.click-year2020').click(function(){
	//
  //    $('.nav-pl-js li').eq(14).click()
  //    return false
	//
  //    })
  //    $('.click-year2021').click(function(){
	//
  //    $('.nav-pl-js li').eq(18).click()
  //    return false
	//
  //    })
  //    $('.click-year2022').click(function(){
	//
  //    $('.nav-pl-js li').eq(19).click()
  //    return false
	//
  //    })
	//
  //       })
	//
	//
	//
	// };
}

// $.ajax('http://ip-api.com/json')
// 	.then(response => {
// 		if (response.country !== 'Australia') {
// 				$('.document-country').fadeOut();
// 		}
// 	})
//
// if (localStorage.getItem('cookie')) {
// 	$('.box-cookie').fadeOut();
// 	$('body').removeClass('cookie');
// }



$(window).bind('load', handler);
$(window).bind('resize', handler);
